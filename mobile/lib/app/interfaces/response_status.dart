class ResponseStatus {
  int statusCode;
  String message;
  Map<String, dynamic>? data;

  ResponseStatus({required this.statusCode, required this.message, this.data});
}
