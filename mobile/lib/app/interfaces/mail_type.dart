class MailType {
  String key;
  int code;
  String mail;

  MailType(this.key, this.code, this.mail);
}
