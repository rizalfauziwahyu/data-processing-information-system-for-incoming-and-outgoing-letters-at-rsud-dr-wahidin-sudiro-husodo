abstract class MailAbstract {
  static const officialNoteEmail = 'OFFICIALNOTEMAIL';
  static const taskOrderEmail = 'TASKORDERMAIL';
  static const invitationEmail = 'INVITATIONMAIL';
  static const circularEmail = 'CIRCULARMAIL';
  static const activityParticipation = 'ACTIVITYPARTICIPATION';
}
