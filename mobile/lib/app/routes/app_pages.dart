import 'package:get/get.dart';

import '../modules/admin_dashboard/bindings/admin_dashboard_binding.dart';
import '../modules/admin_dashboard/views/admin_dashboard_view.dart';
import '../modules/archive/bindings/archive_binding.dart';
import '../modules/archive/views/archive_view.dart';
import '../modules/dashboard/bindings/dashboard_binding.dart';
import '../modules/dashboard/views/dashboard_view.dart';
import '../modules/draft/bindings/draft_binding.dart';
import '../modules/draft/views/draft_view.dart';
import '../modules/edit_letter/bindings/edit_letter_binding.dart';
import '../modules/edit_letter/views/edit_letter_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/incoming/bindings/incoming_binding.dart';
import '../modules/incoming/views/incoming_view.dart';
import '../modules/incoming_detail/bindings/incoming_detail_binding.dart';
import '../modules/incoming_detail/views/incoming_detail_view.dart';
import '../modules/letter/bindings/letter_binding.dart';
import '../modules/letter/views/letter_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/outgoing/bindings/outgoing_binding.dart';
import '../modules/outgoing/views/outgoing_view.dart';
import '../modules/outgoing_detail/bindings/outgoing_detail_binding.dart';
import '../modules/outgoing_detail/views/outgoing_detail_view.dart';
import '../modules/register/bindings/register_binding.dart';
import '../modules/register/views/register_view.dart';
import '../modules/verification_detail/bindings/verification_detail_binding.dart';
import '../modules/verification_detail/views/verification_detail_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => const DashboardView(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: _Paths.LETTER,
      page: () => const LetterView(),
      binding: LetterBinding(),
    ),
    GetPage(
      name: _Paths.INCOMING,
      page: () => const IncomingView(),
      binding: IncomingBinding(),
    ),
    GetPage(
      name: _Paths.INCOMING_DETAIL,
      page: () => const IncomingDetailView(),
      binding: IncomingDetailBinding(),
    ),
    GetPage(
      name: _Paths.OUTGOING,
      page: () => const OutgoingView(),
      binding: OutgoingBinding(),
    ),
    GetPage(
      name: _Paths.OUTGOING_DETAIL,
      page: () => const OutgoingDetailView(),
      binding: OutgoingDetailBinding(),
    ),
    GetPage(
      name: _Paths.DRAFT,
      page: () => const DraftView(),
      binding: DraftBinding(),
    ),
    GetPage(
      name: _Paths.ADMIN_DASHBOARD,
      page: () => const AdminDashboardView(),
      binding: AdminDashboardBinding(),
    ),
    GetPage(
      name: _Paths.VERIFICATION_DETAIL,
      page: () => const VerificationDetailView(),
      binding: VerificationDetailBinding(),
    ),
    GetPage(
      name: _Paths.ARCHIVE,
      page: () => const ArchiveView(),
      binding: ArchiveBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_LETTER,
      page: () => const EditLetterView(),
      binding: EditLetterBinding(),
    ),
  ];
}
