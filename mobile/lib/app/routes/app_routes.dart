// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const LETTER = _Paths.LETTER;
  static const INCOMING = _Paths.INCOMING;
  static const INCOMING_DETAIL = _Paths.INCOMING_DETAIL;
  static const OUTGOING = _Paths.OUTGOING;
  static const OUTGOING_DETAIL = _Paths.OUTGOING_DETAIL;
  static const DRAFT = _Paths.DRAFT;
  static const ADMIN_DASHBOARD = _Paths.ADMIN_DASHBOARD;
  static const VERIFICATION_DETAIL = _Paths.VERIFICATION_DETAIL;
  static const ARCHIVE = _Paths.ARCHIVE;
  static const EDIT_LETTER = _Paths.EDIT_LETTER;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const DASHBOARD = '/dashboard';
  static const LETTER = '/letter';
  static const INCOMING = '/incoming';
  static const INCOMING_DETAIL = '/incoming-detail';
  static const OUTGOING = '/outgoing';
  static const OUTGOING_DETAIL = '/outgoing-detail';
  static const DRAFT = '/draft';
  static const ADMIN_DASHBOARD = '/admin-dashboard';
  static const VERIFICATION_DETAIL = '/verification-detail';
  static const ARCHIVE = '/archive';
  static const EDIT_LETTER = '/edit-letter';
}
