import 'package:app_surat/app/model/mail_model.dart';
import 'package:get/get.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/interfaces/response_status.dart';

class IncomingController extends GetxController {
  RxBool isLoading = true.obs;
  Mails? mails;

  @override
  void onInit() {
    super.onInit();
    getMailInboxes();
  }

  void getMailInboxes() async {
    try {
      ResponseStatus result = await get('mails/inboxes');
      if (result.data != null) {
        mails = Mails.fromJson(result.data!);
      }
    } catch (err) {
      print(err.toString());
    } finally {
      isLoading.value = false;
    }
  }
}
