import 'package:get/get.dart';

import '../controllers/incoming_controller.dart';

class IncomingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IncomingController>(
      () => IncomingController(),
    );
  }
}
