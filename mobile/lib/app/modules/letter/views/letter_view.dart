import 'package:app_surat/app/components/app_bar.dart';
import 'package:app_surat/app/modules/letter/views/activity_participation_mail.dart';
import 'package:app_surat/app/modules/letter/views/invitation_mail.dart';
import 'package:app_surat/app/modules/letter/views/official_note_mail.dart';
import 'package:app_surat/app/modules/letter/views/task_order_mail.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import './circular_mail.dart';
import '../../../interfaces/mail_abstract.dart';
import '../controllers/letter_controller.dart';

class LetterView extends GetView<LetterController> {
  const LetterView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: customAppBar('Tulis Surat'),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
        child: SingleChildScrollView(
          child: _checkEmailType(),
        ),
      ),
    );
  }

  _checkEmailType() {
    switch (controller.mailType.value) {
      case MailAbstract.circularEmail:
        {
          return CircularMail();
        }
      case MailAbstract.taskOrderEmail:
        {
          return TaskOrderMail();
        }
      case MailAbstract.invitationEmail:
        {
          return InvitationMail();
        }
      case MailAbstract.activityParticipation:
        {
          return ActivityParticipationMail();
        }
      case MailAbstract.officialNoteEmail:
        {
          return OfficialNoteMail();
        }
      default:
        {
          return const NotFoundMail();
        }
    }
  }
}

class NotFoundMail extends StatelessWidget {
  const NotFoundMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Tipe Surat tidak Ditemukan'),
    );
  }
}
