import 'package:app_surat/app/interfaces/circular_mail_abstract.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/model/user_model.dart';
import 'package:get/get.dart';

import '../../../interfaces/response_status.dart';
import '../../../routes/app_pages.dart';

class LetterController extends GetxController {
  final List<String> traits = [
    'BIASA',
    'RAHASIA',
    'SANGAT_RAHASIA',
    'KONFIDENSIAL',
  ];

  late List<User> users = [];

  final circularMail = CircularMailAbstract().obs;
  RxString mailType = ''.obs;
  RxBool isLoading = false.obs;
  RxBool loadingUser = true.obs;
  RxInt participantCount = 1.obs;

  RxMap<String, dynamic> formData = {
    'reference_number': '',
    'trait': null,
    'type': '',
    'status': '',
    'subject': '',
    'recipientId': null, // Ganti
    'circularMail': {'content': ''},
    'taskOrderMail': {'content': ''},
    'invitationMail': {
      'date': DateTime.now(),
      'startTime': DateTime.now(),
      'endTime': DateTime.now(),
      'place': '',
      'clothes': '',
      'content': ''
    },
    'officialNoteMail': [
      {'item': '', 'quantity': 0, 'unit': ''}
    ],
    'activityParticipationMail': {'followUpFrom': '', 'content': ''}
  }.obs;

  @override
  void onInit() {
    super.onInit();
    getListUsers();
    mailType.value = Get.arguments.toString();
    onFormChange('type', Get.arguments.toString());
  }

  void onFormChange(key, value) {
    formData[key] = value;
  }

  void onCircularMailChange(key, value) {
    formData['circularMail'][key] = value;
  }

  void onTaskOrderMailChange(key, value) {
    formData['taskOrderMail'][key] = value;
  }

  void onInvitationMailChange(key, value) {
    formData['invitationMail'][key] = value;
  }

  void onOfficialMailChange(key, index, value) {
    formData['officialNoteMail'][index][key] = value;
  }

  void onParticipationMail(key, value) {
    formData['activityParticipationMail'][key] = value;
  }

  void addParticipation() {
    participantCount.value += 1;
    formData['officialNoteMail'].add({'item': '', 'quantity': 0, 'unit': ''});
  }

  void deleteParticipation() {
    if (participantCount.value > 1) {
      formData['officialNoteMail'].removeAt(participantCount.value - 1);
      participantCount.value -= 1;
    }
  }

  void getListUsers() async {
    try {
      ResponseStatus response = await get('users/all');
      if (response.data != null) {
        response.data?['data'].forEach((user) {
          users.add(User.fromJson(user));
        });
      }
      loadingUser.value = false;
    } catch (err) {
      print(err.toString());
      isLoading.value = false;
    }
  }

  void sentEmail() async {
    try {
      isLoading.value = true;
      onFormChange('status', 'PENDING');

      Map<String, dynamic> submittedForm = {...formData};
      submittedForm['recipientId'] = int.parse(submittedForm['recipientId']);

      ResponseStatus response = await post('mails/sent', submittedForm);
      if (response.statusCode == 201) Get.offAllNamed(Routes.DASHBOARD);
      isLoading.value = false;
    } catch (err) {
      print(err.toString());
      isLoading.value = false;
    }
  }

  void saveAsDraft() async {
    try {
      isLoading.value = true;
      onFormChange('status', 'DRAFT');

      Map<String, dynamic> submittedForm = {...formData};
      submittedForm['recipientId'] = int.parse(submittedForm['recipientId']);

      ResponseStatus response = await post('mails/draft', submittedForm);
      if (response.statusCode == 201) Get.offAllNamed(Routes.DASHBOARD);
      isLoading.value = false;
    } catch (err) {
      print(err.toString());
      isLoading.value = false;
    }
  }
}
