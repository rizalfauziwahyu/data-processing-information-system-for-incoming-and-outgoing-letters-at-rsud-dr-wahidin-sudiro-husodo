import 'package:app_surat/app/interfaces/response_status.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/model/mail_model.dart';
import 'package:get/get.dart';

class ArchiveController extends GetxController {
  RxBool isLoading = true.obs;
  List<AdminMail>? mails;

  @override
  void onInit() {
    super.onInit();
    getMailInboxes();
  }

  void getMailInboxes() async {
    try {
      ResponseStatus result = await get('mails/archieved');
      if (result.data != null) {
        mails = <AdminMail>[];
        await result.data?['data'].forEach((v) {
          mails?.add(AdminMail.fromJson(v));
        });
      }
    } catch (err) {
      print(err.toString());
    } finally {
      isLoading.value = false;
    }
  }
}
