import 'package:get/get.dart';

import '../../../global/api_config.dart' as globals;
import '../../../interfaces/response_status.dart';
import '../../../methods/auth.dart';
import '../../../routes/app_pages.dart';

class LoginController extends GetxController {
  RxString usernameData = 'ilham'.obs;
  RxString passwordData = 'ilham'.obs;
  RxString errorData = ''.obs;

  RxBool isPasswordhidden = true.obs;
  RxBool isLoading = false.obs;

  void changePasswordVisibility() =>
      isPasswordhidden.value = !isPasswordhidden.value;

  void onchangeUsername(String username) => usernameData.value = username;
  void onchangePassword(String password) => passwordData.value = password;

  void loginUser() async {
    try {
      isLoading(true);
      errorData.value = '';

      ResponseStatus response =
          await login(usernameData.value, passwordData.value, globals.url);
      errorData.value = response.message;
      if (response.statusCode == 200) {
        print(response.data);
        if (globals.user?.role == 'ADMIN') {
          Get.offAllNamed(Routes.ADMIN_DASHBOARD);
        } else {
          Get.offAllNamed(Routes.DASHBOARD);
        }
      }
    } catch (err) {
      errorData.value = err.toString();
    } finally {
      isLoading(false);
    }
  }

  void routeRegisterView() {
    Get.offAllNamed(Routes.REGISTER);
  }
}
