import 'package:app_surat/app/components/app_bar.dart';
import 'package:app_surat/app/components/card_letter.dart';
import 'package:app_surat/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/admin_dashboard_controller.dart';

class AdminDashboardView extends GetView<AdminDashboardController> {
  const AdminDashboardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double cWidth = MediaQuery.of(context).size.width * 0.065;
    return Scaffold(
      appBar: customAppBar('Dashboard'),
      backgroundColor: const Color.fromARGB(255, 72, 89, 119),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                color: Color.fromRGBO(46, 60, 85, 1),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
              ),
              padding: const EdgeInsets.all(13),
              child: const Padding(
                padding:
                    EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Pengirim / Asal Surat',
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 4),
                        SizedBox(
                          child: Text(
                            'Bagian Umum RSUD',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    CircleAvatar(
                      backgroundColor: Color.fromARGB(255, 238, 233, 232),
                      child: Text(
                        'AH',
                        style: TextStyle(
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: cWidth),
            const Center(
              child: Text(
                'Permintaan Verifikasi Surat',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: cWidth),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: Obx(
                () => controller.isLoading.value
                    ? const Center(child: Text('Loading'))
                    : Column(children: <Widget>[..._mailCardList(context)]),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _mailCardList(BuildContext context) {
    return controller.mails!
        .map(
          (mail) => Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Column(
              children: [
                cardLetter(
                    context,
                    mail.subject!,
                    mail.referenceNumber!,
                    mail.trait!,
                    mail.createdAt!,
                    mail.status!,
                    () => {
                          Get.toNamed(Routes.VERIFICATION_DETAIL,
                              arguments: {'type': mail.type, 'id': mail.id})
                        }),
                Container(
                  height: 1,
                  color: const Color.fromRGBO(30, 41, 60, 1),
                ),
              ],
            ),
          ),
        )
        .toList();
  }
}
