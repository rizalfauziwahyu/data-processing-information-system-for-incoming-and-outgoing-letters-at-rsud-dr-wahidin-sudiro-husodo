import 'package:get/get.dart';

import '../controllers/outgoing_controller.dart';

class OutgoingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutgoingController>(
      () => OutgoingController(),
    );
  }
}
