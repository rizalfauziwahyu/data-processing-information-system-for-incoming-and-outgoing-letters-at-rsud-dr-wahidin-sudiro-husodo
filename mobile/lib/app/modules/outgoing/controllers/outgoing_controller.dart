import 'package:app_surat/app/model/mail_model.dart';
import 'package:get/get.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/interfaces/response_status.dart';

class OutgoingController extends GetxController {
  RxBool isLoading = true.obs;
  Mails? mails;

  @override
  void onInit() {
    super.onInit();
    getMailSents();
  }

  void getMailSents() async {
    try {
      ResponseStatus result = await get('mails/sents');
      if (result.data != null) {
        mails = Mails.fromJson(result.data!);
      }
    } catch (err) {
      print(err.toString());
    } finally {
      isLoading.value = false;
    }
  }
}
