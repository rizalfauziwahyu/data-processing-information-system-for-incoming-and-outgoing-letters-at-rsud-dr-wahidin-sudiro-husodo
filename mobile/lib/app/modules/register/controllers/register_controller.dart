import 'package:get/get.dart';

import '../../../global/api_config.dart' as globals;
import '../../../interfaces/response_status.dart';
import '../../../methods/auth.dart';
import '../../../routes/app_pages.dart';

class RegisterController extends GetxController {
  RxString usernameData = ''.obs;
  RxString passwordData = ''.obs;
  RxString nameData = ''.obs;
  RxString errorData = ''.obs;

  RxBool isPasswordhidden = true.obs;
  RxBool isLoading = false.obs;

  void changePasswordVisibility() =>
      isPasswordhidden.value = !isPasswordhidden.value;

  void onchangeUsername(String username) => usernameData.value = username;
  void onchangePassword(String password) => passwordData.value = password;
  void onchangeName(String name) => nameData.value = name;

  void registerUser() async {
    try {
      isLoading(true);
      errorData.value = '';

      ResponseStatus response = await register(usernameData.value,
          passwordData.value, nameData.value, "USER", globals.url);
      errorData.value = response.message;
      if (response.statusCode == 201) Get.offAllNamed(Routes.DASHBOARD);
    } catch (err) {
      errorData.value = err.toString();
    } finally {
      isLoading(false);
    }
  }

  void routeLoginView() {
    Get.offAllNamed(Routes.LOGIN);
  }
}
