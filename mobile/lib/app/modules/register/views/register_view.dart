import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  const RegisterView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('Authentication'),
        backgroundColor: const Color.fromRGBO(30, 41, 60, 1),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: 124),
            const Center(
              child: Text(
                'Register',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w600,
                    color: Color.fromRGBO(30, 41, 60, 1)),
              ),
            ),
            const SizedBox(height: 24),
            const SizedBox(height: 20),
            TextFormField(
              initialValue: controller.usernameData.value,
              onChanged: (value) => controller.onchangeUsername(value),
              decoration: InputDecoration(
                hintText: 'Username',
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Obx(
              () => TextFormField(
                initialValue: controller.passwordData.value,
                obscureText: controller.isPasswordhidden.value,
                onChanged: (value) => controller.onchangePassword(value),
                decoration: InputDecoration(
                  hintText: 'Password',
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  suffixIcon: IconButton(
                    onPressed: () {
                      controller.changePasswordVisibility();
                    },
                    icon: const Icon(
                      Icons.remove_red_eye,
                      color: Color.fromRGBO(30, 41, 60, 1),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            TextFormField(
              initialValue: controller.usernameData.value,
              onChanged: (value) => controller.onchangeName(value),
              decoration: InputDecoration(
                hintText: 'Name',
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Obx(
              () => Center(
                child: controller.errorData.isNotEmpty
                    ? Container(
                        margin: const EdgeInsets.only(bottom: 2),
                        child: Text(
                          controller.errorData.value,
                          style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(202, 32, 43, 1)),
                        ),
                      )
                    : null,
              ),
            ),
            const SizedBox(height: 16),
            Center(
              child: RichText(
                text: TextSpan(
                  text: "Already have an account, ",
                  style: const TextStyle(
                      color: Color.fromRGBO(30, 41, 60, 1), fontSize: 16),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Login',
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            controller.routeLoginView();
                          }),
                    const TextSpan(text: ' here!'),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20),
            Obx(
              () => MaterialButton(
                onPressed: () {
                  controller.registerUser();
                },
                color: const Color.fromRGBO(30, 41, 60, 1),
                child: controller.isLoading.value
                    ? const SizedBox(
                        height: 10.0,
                        width: 10.0,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                        ),
                      )
                    : const Text(
                        'Submit',
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
