import 'package:get/get.dart';

import '../../../interfaces/mail_abstract.dart';
import '../../../interfaces/mail_type.dart';

class DashboardController extends GetxController {
  final selectedMail = 'OFFICIALNOTEMAIL'.obs;

  List<Map<String, dynamic>> mailList = [
    {'key': MailAbstract.officialNoteEmail, 'code': 033, 'mail': 'Nota Dinas'},
    {'key': MailAbstract.taskOrderEmail, 'code': 033, 'mail': 'Perintah Tugas'},
    {'key': MailAbstract.circularEmail, 'code': 033, 'mail': 'Edaran'},
    {
      'key': MailAbstract.activityParticipation,
      'code': 033,
      'mail': 'Partisipasi Kegiatan'
    },
    {'key': MailAbstract.invitationEmail, 'code': 033, 'mail': 'Undangan'}
  ];

  List<MailType> mailTypes = [];

  @override
  void onInit() {
    super.onInit();
    mapMailType();
  }

  void mapMailType() {
    for (var mail in mailList) {
      mailTypes.add(MailType(mail['key'], mail['code'], mail['mail']));
    }
  }
}
