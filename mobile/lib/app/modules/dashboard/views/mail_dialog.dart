import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../routes/app_pages.dart';
import '../controllers/dashboard_controller.dart';

showMailDialog(context) {
  final controller = Get.find<DashboardController>();
  const double spacing = 20;
  const double padding = 24;

  showDialog(
    context: context,
    builder: (BuildContext context) {
      MediaQueryData media = MediaQuery.of(context);
      return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          constraints: BoxConstraints(
              minHeight: media.size.height * 3 / 4.5,
              maxHeight: media.size.height * 3 / 4.5),
          child: Padding(
            padding: const EdgeInsets.all(padding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Wrap(
                  spacing: spacing,
                  runSpacing: spacing,
                  children: controller.mailTypes.map<Widget>(
                    (mail) {
                      return Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              controller.selectedMail.value = mail.key;
                            },
                            child: Obx(
                              () => Container(
                                width: media.size.width / 2.5 -
                                    (spacing * 2) -
                                    (padding * 2),
                                height: media.size.width / 2.5 -
                                    (spacing * 2) -
                                    (padding * 2),
                                decoration: BoxDecoration(
                                  color: controller.selectedMail.value ==
                                          mail.key
                                      ? const Color.fromRGBO(62, 78, 106, 1)
                                      : const Color.fromRGBO(224, 227, 227, 1),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(100)),
                                ),
                                padding: const EdgeInsets.all(13),
                                child: Center(
                                  child: FittedBox(
                                    fit: BoxFit.cover,
                                    child: Text(
                                      mail.code.toString().padLeft(3, '0'),
                                      style: TextStyle(
                                        color: controller.selectedMail.value ==
                                                mail.key
                                            ? const Color.fromRGBO(
                                                224, 227, 227, 1)
                                            : const Color.fromRGBO(
                                                30, 41, 60, 1),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 6),
                          const Text(
                            'Surat',
                            style: TextStyle(
                                color: Color.fromRGBO(30, 41, 60, 1),
                                fontSize: 10),
                          ),
                          SizedBox(
                            width: media.size.width / 2.5 -
                                (spacing * 2) -
                                (padding * 2),
                            height: 30,
                            child: Text(
                              '-${mail.mail}-',
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  color: Color.fromRGBO(30, 41, 60, 1),
                                  fontSize: 12),
                            ),
                          ),
                        ],
                      );
                    },
                  ).toList(),
                ),
                const SizedBox(height: 30),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(30, 41, 60, 1),
                    minimumSize: const Size.fromHeight(50),
                  ),
                  onPressed: () {
                    Get.toNamed(Routes.LETTER,
                        arguments: controller.selectedMail);
                  },
                  child: const Text(
                    'Pilih',
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
