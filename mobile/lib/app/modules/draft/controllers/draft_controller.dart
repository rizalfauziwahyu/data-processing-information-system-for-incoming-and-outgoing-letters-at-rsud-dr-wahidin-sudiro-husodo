import 'package:app_surat/app/model/mail_model.dart';
import 'package:get/get.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/interfaces/response_status.dart';

class DraftController extends GetxController {
  RxBool isLoading = true.obs;
  List<DetailMail> mails = [];

  @override
  void onInit() {
    super.onInit();
    getDraftMails();
  }

  void getDraftMails() async {
    try {
      ResponseStatus result = await get('mails');
      if (result.data != null) {
        result.data?['data'].forEach((v) {
          mails.add(DetailMail.fromJson(v));
        });
      }
    } catch (err) {
      print(err.toString());
    } finally {
      isLoading.value = false;
    }
  }
}
