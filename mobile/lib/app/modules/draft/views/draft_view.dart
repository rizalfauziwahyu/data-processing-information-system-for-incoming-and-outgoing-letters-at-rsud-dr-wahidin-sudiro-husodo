import 'package:app_surat/app/components/card_letter.dart';
import 'package:app_surat/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/draft_controller.dart';

class DraftView extends GetView<DraftController> {
  const DraftView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Draft / Revisi'),
        backgroundColor: const Color.fromRGBO(30, 41, 60, 1),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: 'Telusuri',
                  hintStyle: const TextStyle(
                    fontSize: 12,
                    color: Color.fromRGBO(30, 41, 60, 1),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                    borderRadius: BorderRadius.circular(4),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Obx(
                () => controller.isLoading.value
                    ? const Center(child: Text('Loading'))
                    : Column(children: <Widget>[..._mailCardList(context)]),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _mailCardList(BuildContext context) {
    return controller.mails
        .map(
          (mail) => Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Column(
              children: [
                cardLetter(
                    context,
                    mail.subject!,
                    mail.referenceNumber!,
                    mail.trait!,
                    mail.createdAt!,
                    mail.status!,
                    () => {
                          Get.toNamed(Routes.INCOMING_DETAIL,
                              arguments: {'type': mail.type, 'id': mail.id})
                        }),
                Container(
                  height: 1,
                  color: const Color.fromRGBO(30, 41, 60, 1),
                ),
              ],
            ),
          ),
        )
        .toList();
  }
}
