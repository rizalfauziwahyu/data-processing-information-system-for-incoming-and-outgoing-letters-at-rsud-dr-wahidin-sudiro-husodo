import 'package:get/get.dart';

import '../controllers/verification_detail_controller.dart';

class VerificationDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VerificationDetailController>(
      () => VerificationDetailController(),
    );
  }
}
