import 'package:app_surat/app/modules/verification_detail/controllers/verification_detail_controller.dart';
import 'package:app_surat/app/modules/verification_detail/views/revise_modal.dart';
import 'package:app_surat/app/modules/verification_detail/views/verify_modal.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class VerificationParticipationMail
    extends GetView<VerificationDetailController> {
  final double verticalGap = 20.0;
  final double horizontalGap = 8.0;

  const VerificationParticipationMail({super.key});

  @override
  Widget build(BuildContext context) {
    double cWidth = MediaQuery.of(context).size.width * 0.6;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          const Center(
            child: Text(
              'Surat Partisipasi Kegiatan',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(height: 20),
          Container(
            decoration: const BoxDecoration(
                color: Color.fromRGBO(224, 227, 227, 1),
                borderRadius: BorderRadius.all(Radius.circular(2))),
            padding: const EdgeInsets.all(13),
            child: Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.brown.shade800,
                  child: const Text('A'),
                ),
                SizedBox(width: horizontalGap),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      'Pengirim / Asal Surat',
                      style: TextStyle(
                        fontSize: 12,
                        color: Color.fromRGBO(30, 41, 60, 1),
                      ),
                    ),
                    const SizedBox(height: 4),
                    SizedBox(
                      width: cWidth,
                      child: Text(
                        controller.detailMail.from!,
                        style: const TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: verticalGap),
          Container(
            decoration: const BoxDecoration(
                color: Color.fromRGBO(224, 227, 227, 1),
                borderRadius: BorderRadius.all(Radius.circular(2))),
            padding: const EdgeInsets.all(13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Text(
                  'Tujuan',
                  style: TextStyle(
                    fontSize: 10,
                    color: Color.fromRGBO(30, 41, 60, 1),
                  ),
                ),
                const SizedBox(height: 4),
                SizedBox(
                  width: cWidth,
                  child: Text(
                    controller.detailMail.recipient!,
                    style: const TextStyle(
                      fontSize: 14,
                      color: Color.fromRGBO(30, 41, 60, 1),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: verticalGap),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Tindak Lanjut',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 6),
              TextFormField(
                initialValue: controller
                    .detailMail.activityParticipationMail!.followUpFrom,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                keyboardType: TextInputType.text,
                validator: (value) {
                  if (GetUtils.isNullOrBlank(value!) == false) {
                    return null;
                  }
                  return "Tulis Tindak Lanjut";
                },
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: 'Tulis Tindak Lanjut',
                  hintStyle: const TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  filled: true,
                  fillColor: const Color.fromRGBO(224, 227, 227, 1),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: verticalGap),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Isi Surat',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 6),
              TextFormField(
                initialValue:
                    controller.detailMail.activityParticipationMail!.content,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: 'Placeholder',
                  hintStyle: const TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  filled: true,
                  fillColor: const Color.fromRGBO(224, 227, 227, 1),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
                minLines:
                    6, // any number you need (It works as the rows for the textarea)
                keyboardType: TextInputType.multiline,
                maxLines: null,
              ),
            ],
          ),
          SizedBox(height: verticalGap),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Lampiran',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 6),
              TextFormField(
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: 'Lampiran Tidak Tersedia',
                  hintStyle: const TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: verticalGap),
          MaterialButton(
            onPressed: () {
              showVerifyModal(context, controller.verifiyMail);
            },
            color: const Color.fromARGB(255, 154, 133, 212),
            child: const Text(
              'Verifikasi',
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
          ),
          MaterialButton(
            onPressed: () {
              showReviseModal(context, controller.reviseModal);
            },
            color: const Color.fromARGB(255, 92, 180, 70),
            child: const Text(
              'Revisi',
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
          ),
          MaterialButton(
            onPressed: () {
              Get.back();
            },
            color: const Color.fromARGB(255, 37, 52, 80),
            child: const Text(
              'Kembali',
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
          ),
          SizedBox(height: verticalGap),
        ],
      ),
    );
  }
}
