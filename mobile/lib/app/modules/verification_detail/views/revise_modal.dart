import 'package:flutter/material.dart';
import 'package:get/get_utils/src/get_utils/get_utils.dart';

showReviseModal(context, Function onClick) {
  String revise = '';
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          constraints: const BoxConstraints(maxHeight: 420),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 92, 180, 70),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(4),
                        topRight: Radius.circular(4))),
                padding: const EdgeInsets.all(16),
                child: const Center(
                  child: Text(
                    'Ajukan Revisi',
                    style: TextStyle(
                        color: Color.fromARGB(255, 235, 235, 235),
                        fontSize: 16),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 24.0, top: 16.0, right: 24.0, bottom: 24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    const Center(
                      child: Text(
                        'Tulis yang Perlu Diubah / Diperbaiki',
                        style: TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                    ),
                    const SizedBox(height: 14),
                    TextFormField(
                      validator: (value) {
                        if (GetUtils.isNullOrBlank(value!) == false) {
                          return null;
                        }
                        return "Enter Text";
                      },
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(10.0),
                        hintText: 'Tulis Perbaikan',
                        hintStyle: const TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(30, 41, 60, 1)),
                          borderRadius: BorderRadius.circular(3),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color.fromRGBO(30, 41, 60, 1)),
                          borderRadius: BorderRadius.circular(3),
                        ),
                      ),
                      minLines:
                          6, // any number you need (It works as the rows for the textarea)
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      onChanged: (value) {
                        revise = value;
                      },
                    ),
                    const SizedBox(height: 14),
                    MaterialButton(
                      onPressed: () {
                        onClick(revise);
                      },
                      color: const Color.fromARGB(255, 92, 180, 70),
                      child: const Text(
                        'Revisi',
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ),
                    const SizedBox(height: 4),
                    MaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: const Color.fromARGB(255, 186, 189, 195),
                      child: const Text(
                        'Kembali',
                        style: TextStyle(
                            color: Color.fromARGB(255, 84, 84, 84),
                            fontSize: 14),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
