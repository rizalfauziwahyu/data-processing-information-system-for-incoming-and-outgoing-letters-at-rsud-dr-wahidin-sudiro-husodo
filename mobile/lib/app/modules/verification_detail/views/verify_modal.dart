import 'package:flutter/material.dart';

showVerifyModal(context, Function onClick) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          constraints: const BoxConstraints(maxHeight: 320),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 154, 133, 212),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(4),
                        topRight: Radius.circular(4))),
                padding: const EdgeInsets.all(16),
                child: const Center(
                  child: Text(
                    'Verifikasi Surat',
                    style: TextStyle(
                        color: Color.fromARGB(255, 235, 235, 235),
                        fontSize: 16),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 24.0, top: 16.0, right: 24.0, bottom: 24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    const Center(
                      child: Text(
                        'Apakah anda yakin akan MEMVERIFIKASI surat ini',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                    ),
                    const SizedBox(height: 6),
                    const Center(
                      child: Text(
                        'Surat akan diperbolehkan untuk dikirim ke tujuan',
                        style: TextStyle(
                          fontSize: 10,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                    ),
                    const SizedBox(height: 14),
                    MaterialButton(
                      onPressed: () {
                        onClick();
                      },
                      color: const Color.fromARGB(255, 154, 133, 212),
                      child: const Text(
                        'Verifikasi',
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ),
                    const SizedBox(height: 4),
                    MaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: const Color.fromARGB(255, 186, 189, 195),
                      child: const Text(
                        'Kembali',
                        style: TextStyle(
                            color: Color.fromARGB(255, 84, 84, 84),
                            fontSize: 14),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
