import 'package:app_surat/app/components/app_bar.dart';
import 'package:app_surat/app/interfaces/mail_abstract.dart';
import 'package:app_surat/app/modules/verification_detail/views/verification_circular_mail.dart';
import 'package:app_surat/app/modules/verification_detail/views/verification_invitation_mail.dart';
import 'package:app_surat/app/modules/verification_detail/views/verification_official_note_mail.dart';
import 'package:app_surat/app/modules/verification_detail/views/verification_participation_mail.dart';
import 'package:app_surat/app/modules/verification_detail/views/vierification_task_order_mail.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/verification_detail_controller.dart';

class VerificationDetailView extends GetView<VerificationDetailController> {
  const VerificationDetailView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: customAppBar('Detail Surat Masukk'),
      backgroundColor: const Color.fromARGB(255, 72, 89, 119),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
          child: controller.isLoading.value
              ? const LoadingMail()
              : _checkEmailType(),
        ),
      ),
    );
  }

  Widget _checkEmailType() {
    switch (controller.mailType.value) {
      case MailAbstract.circularEmail:
        {
          return const VerificationCircularMail();
        }
      case MailAbstract.taskOrderEmail:
        {
          return const VerificationTaskOrderMail();
        }
      case MailAbstract.invitationEmail:
        {
          return const VerificationInvitationMail();
        }
      case MailAbstract.activityParticipation:
        {
          return const VerificationParticipationMail();
        }
      case MailAbstract.officialNoteEmail:
        {
          return const VerificationOfficialNoteMail();
        }
      default:
        {
          return const NotFoundMail();
        }
    }
  }
}

class LoadingMail extends StatelessWidget {
  const LoadingMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Loading'),
    );
  }
}

class NotFoundMail extends StatelessWidget {
  const NotFoundMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Tipe Surat tidak Ditemukan'),
    );
  }
}
