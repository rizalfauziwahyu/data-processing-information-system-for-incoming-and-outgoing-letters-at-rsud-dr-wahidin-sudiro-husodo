import 'package:app_surat/app/components/app_bar.dart';
import 'package:app_surat/app/interfaces/mail_abstract.dart';
import 'package:app_surat/app/modules/outgoing_detail/views/view_circular_mail.dart';
import 'package:app_surat/app/modules/outgoing_detail/views/view_invitation_mail.dart';
import 'package:app_surat/app/modules/outgoing_detail/views/view_official_note_mail.dart';
import 'package:app_surat/app/modules/outgoing_detail/views/view_participation_mail.dart';
import 'package:app_surat/app/modules/outgoing_detail/views/view_task_order_mail.dart';
import 'package:app_surat/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/outgoing_detail_controller.dart';

class OutgoingDetailView extends GetView<OutgoingDetailController> {
  const OutgoingDetailView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar('Detail Surat Keluar'),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Obx(
          () => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 25),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  controller.isLoading.value
                      ? const LoadingMail()
                      : _checkEmailType(),
                  const SizedBox(height: 16),
                  Obx(
                    () => controller.isLoading.value == false &&
                            controller.detailMail.status == 'REVISION'
                        ? MaterialButton(
                            onPressed: () {
                              Get.offAndToNamed(Routes.EDIT_LETTER, arguments: {
                                'id': controller.mailId,
                                'type': controller.mailType
                              });
                            },
                            color: const Color.fromARGB(255, 114, 72, 44),
                            child: const Text(
                              'Revisi',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          )
                        : const Text(''),
                  ),
                  MaterialButton(
                    onPressed: () {
                      Get.back();
                    },
                    color: const Color.fromRGBO(30, 41, 60, 1),
                    child: const Text(
                      'Kembali',
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    ),
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _checkEmailType() {
    switch (controller.mailType.value) {
      case MailAbstract.circularEmail:
        {
          return const ViewCircularMail();
        }
      case MailAbstract.taskOrderEmail:
        {
          return const ViewTaskOrderMail();
        }
      case MailAbstract.invitationEmail:
        {
          return const ViewInvitationMail();
        }
      case MailAbstract.activityParticipation:
        {
          return const ViewParticipationMail();
        }
      case MailAbstract.officialNoteEmail:
        {
          return const ViewOfficialNoteMail();
        }
      default:
        {
          return const NotFoundMail();
        }
    }
  }
}

class LoadingMail extends StatelessWidget {
  const LoadingMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Loading'),
    );
  }
}

class NotFoundMail extends StatelessWidget {
  const NotFoundMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Tipe Surat tidak Ditemukan'),
    );
  }
}
