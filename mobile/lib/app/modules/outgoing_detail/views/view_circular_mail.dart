import 'package:app_surat/app/modules/outgoing_detail/controllers/outgoing_detail_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ViewCircularMail extends GetView<OutgoingDetailController> {
  final double verticalGap = 20.0;
  final double horizontalGap = 8.0;

  const ViewCircularMail({super.key});

  @override
  Widget build(BuildContext context) {
    double cWidth = MediaQuery.of(context).size.width * 0.6;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          const Center(
            child: Text(
              'Surat Edaran',
              style: TextStyle(
                  color: Color.fromRGBO(30, 41, 60, 1),
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(height: 20),
          Container(
            decoration: const BoxDecoration(
                color: Color.fromRGBO(224, 227, 227, 1),
                borderRadius: BorderRadius.all(Radius.circular(2))),
            padding: const EdgeInsets.all(13),
            child: Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.brown.shade800,
                  child: const Text('A'),
                ),
                SizedBox(width: horizontalGap),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      'Pengirim / Asal Surat',
                      style: TextStyle(
                        fontSize: 12,
                        color: Color.fromRGBO(30, 41, 60, 1),
                      ),
                    ),
                    const SizedBox(height: 4),
                    SizedBox(
                      width: cWidth,
                      child: Text(
                        controller.detailMail.from!,
                        style: const TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: verticalGap),
          Container(
            decoration: const BoxDecoration(
                color: Color.fromRGBO(224, 227, 227, 1),
                borderRadius: BorderRadius.all(Radius.circular(2))),
            padding: const EdgeInsets.all(13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Text(
                  'Tujuan',
                  style: TextStyle(
                    fontSize: 10,
                    color: Color.fromRGBO(30, 41, 60, 1),
                  ),
                ),
                const SizedBox(height: 4),
                SizedBox(
                  width: cWidth,
                  child: Text(
                    controller.detailMail.recipient!,
                    style: const TextStyle(
                      fontSize: 14,
                      color: Color.fromRGBO(30, 41, 60, 1),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: verticalGap),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Isi Surat',
                style: TextStyle(
                  fontSize: 12,
                  color: Color.fromRGBO(30, 41, 60, 1),
                ),
              ),
              const SizedBox(height: 6),
              TextFormField(
                initialValue: controller.detailMail.circularMail!.content,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(10.0),
                  hintText: 'Placeholder',
                  hintStyle: const TextStyle(
                    fontSize: 12,
                    color: Color.fromRGBO(30, 41, 60, 1),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
                minLines:
                    6, // any number you need (It works as the rows for the textarea)
                keyboardType: TextInputType.multiline,
                maxLines: null,
              ),
            ],
          ),
          SizedBox(height: verticalGap),
        ],
      ),
    );
  }
}
