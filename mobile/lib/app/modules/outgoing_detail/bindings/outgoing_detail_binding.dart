import 'package:get/get.dart';

import '../controllers/outgoing_detail_controller.dart';

class OutgoingDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OutgoingDetailController>(
      () => OutgoingDetailController(),
    );
  }
}
