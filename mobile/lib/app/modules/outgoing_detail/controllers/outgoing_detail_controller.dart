import 'package:app_surat/app/interfaces/response_status.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/model/mail_model.dart';
import 'package:get/get.dart';

class OutgoingDetailController extends GetxController {
  //TODO: Implement OutgoingDetailController

  RxBool isLoading = true.obs;
  RxString mailType = ''.obs;
  RxInt mailId = 0.obs;
  late DetailMail detailMail;

  @override
  void onInit() {
    super.onInit();
    mailType.value = Get.arguments['type'];
    mailId.value = Get.arguments['id'];
    getInitialData();
  }

  void getInitialData() async {
    try {
      ResponseStatus result = await getById('mails', mailId.value.toString());
      if (result.data != null) {
        detailMail = DetailMail.fromJson(result.data!['data']);
      }
      isLoading.value = false;
    } catch (err) {
      print(err.toString());
    }
  }
}
