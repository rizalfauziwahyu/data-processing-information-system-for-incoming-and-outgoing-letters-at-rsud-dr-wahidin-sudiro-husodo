import 'package:get/get.dart';

import '../controllers/incoming_detail_controller.dart';

class IncomingDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IncomingDetailController>(
      () => IncomingDetailController(),
    );
  }
}
