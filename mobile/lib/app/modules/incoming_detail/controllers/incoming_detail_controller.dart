import 'package:app_surat/app/global/api_config.dart';
import 'package:app_surat/app/interfaces/response_status.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/model/mail_model.dart';
import 'package:app_surat/app/routes/app_pages.dart';
import 'package:get/get.dart';

class IncomingDetailController extends GetxController {
  //TODO: Implement IncomingDetailController

  RxBool isLoading = true.obs;
  RxString mailType = ''.obs;
  RxInt mailId = 0.obs;
  late DetailMail detailMail;

  @override
  void onInit() {
    super.onInit();
    mailType.value = Get.arguments['type'];
    mailId.value = Get.arguments['id'];
    getInitialData();
  }

  void getInitialData() async {
    try {
      ResponseStatus result = await getById('mails', mailId.value.toString());
      if (result.data != null) {
        detailMail = DetailMail.fromJson(result.data!['data']);
      }
      isLoading.value = false;
    } catch (err) {
      print(err.toString());
    }
  }

  void archiveMail() async {
    try {
      print(detailMail.id);
      ResponseStatus result =
          await post('mails/archieve-mail', {"mailBoxId": user!.id.toString()});
      if (result.statusCode == 201) {
        Get.offAllNamed(Routes.DASHBOARD);
      }
      isLoading.value = false;
    } catch (err) {
      print(err.toString());
    }
  }
}
