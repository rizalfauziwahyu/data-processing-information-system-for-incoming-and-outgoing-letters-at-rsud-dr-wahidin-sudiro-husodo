import 'package:app_surat/app/modules/incoming_detail/controllers/incoming_detail_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ViewOfficialNoteMail extends GetView<IncomingDetailController> {
  final double verticalGap = 20.0;
  final double horizontalGap = 8.0;

  const ViewOfficialNoteMail({super.key});

  @override
  Widget build(BuildContext context) {
    double cWidth = MediaQuery.of(context).size.width * 0.6;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        const Center(
          child: Text(
            'Surat Dinas',
            style: TextStyle(
                color: Color.fromRGBO(30, 41, 60, 1),
                fontSize: 16,
                fontWeight: FontWeight.bold),
          ),
        ),
        const SizedBox(height: 20),
        Container(
          decoration: const BoxDecoration(
              color: Color.fromRGBO(224, 227, 227, 1),
              borderRadius: BorderRadius.all(Radius.circular(2))),
          padding: const EdgeInsets.all(13),
          child: Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.brown.shade800,
                child: const Text('A'),
              ),
              SizedBox(width: horizontalGap),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    'Pengirim / Asal Surat',
                    style: TextStyle(
                      fontSize: 12,
                      color: Color.fromRGBO(30, 41, 60, 1),
                    ),
                  ),
                  const SizedBox(height: 4),
                  SizedBox(
                    width: cWidth,
                    child: Text(
                      controller.detailMail.from!,
                      style: const TextStyle(
                        fontSize: 14,
                        color: Color.fromRGBO(30, 41, 60, 1),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(height: verticalGap),
        Container(
          decoration: const BoxDecoration(
              color: Color.fromRGBO(224, 227, 227, 1),
              borderRadius: BorderRadius.all(Radius.circular(2))),
          padding: const EdgeInsets.all(13),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Text(
                'Tujuan',
                style: TextStyle(
                  fontSize: 10,
                  color: Color.fromRGBO(30, 41, 60, 1),
                ),
              ),
              const SizedBox(height: 4),
              SizedBox(
                width: cWidth,
                child: Text(
                  controller.detailMail.recipient!,
                  style: const TextStyle(
                    fontSize: 14,
                    color: Color.fromRGBO(30, 41, 60, 1),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: verticalGap),
        Column(
            children:
                controller.detailMail.officialNoteMails!.map<Widget>((mail) {
          return Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Item',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                      const SizedBox(height: 6),
                      TextFormField(
                        initialValue: mail.item,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (GetUtils.isNullOrBlank(value!) == false) {
                            return null;
                          }
                          return "Enter Text";
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10.0),
                          hintText: 'Tulis Nomor Surat',
                          hintStyle: const TextStyle(
                            fontSize: 12,
                            color: Color.fromRGBO(30, 41, 60, 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Jumlah',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                      const SizedBox(height: 6),
                      TextFormField(
                        initialValue: mail.quantity.toString(),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (GetUtils.isNullOrBlank(value!) == true) {
                            return 'Masukkan Angka';
                          } else if (GetUtils.isNumericOnly(value) == false) {
                            return 'Masukkan Angka';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10.0),
                          hintText: 'Tulis Nomor Surat',
                          hintStyle: const TextStyle(
                            fontSize: 12,
                            color: Color.fromRGBO(30, 41, 60, 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Unit',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                      const SizedBox(height: 6),
                      TextFormField(
                        initialValue: mail.unit,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (GetUtils.isNullOrBlank(value!) == false) {
                            return null;
                          }
                          return "Enter Text";
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10.0),
                          hintText: 'Tulis Nomor Surat',
                          hintStyle: const TextStyle(
                            fontSize: 12,
                            color: Color.fromRGBO(30, 41, 60, 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }).toList()),
        SizedBox(height: verticalGap),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Lampiran',
              style: TextStyle(
                fontSize: 12,
                color: Color.fromRGBO(30, 41, 60, 1),
              ),
            ),
            const SizedBox(height: 6),
            TextFormField(
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(10.0),
                hintText: 'Lampiran Tidak Tersedia',
                hintStyle: const TextStyle(
                  fontSize: 12,
                  color: Color.fromRGBO(30, 41, 60, 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: verticalGap),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Disposisikan Surat',
              style: TextStyle(
                fontSize: 12,
                color: Color.fromRGBO(30, 41, 60, 1),
              ),
            ),
            const SizedBox(height: 6),
            TextFormField(
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(10.0),
                hintText: 'Anda belum disposisikan Surat',
                hintStyle: const TextStyle(
                  fontSize: 12,
                  color: Color.fromRGBO(30, 41, 60, 1),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Color.fromRGBO(30, 41, 60, 1)),
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
