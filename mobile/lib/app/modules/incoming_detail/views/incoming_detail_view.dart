import 'package:app_surat/app/components/app_bar.dart';
import 'package:app_surat/app/interfaces/mail_abstract.dart';
import 'package:app_surat/app/modules/incoming_detail/views/archive_modal.dart';
import 'package:app_surat/app/modules/incoming_detail/views/view_circular_mail.dart';
import 'package:app_surat/app/modules/incoming_detail/views/view_invitation_mail.dart';
import 'package:app_surat/app/modules/incoming_detail/views/view_official_note_mail.dart';
import 'package:app_surat/app/modules/incoming_detail/views/view_participation_mail.dart';
import 'package:app_surat/app/modules/incoming_detail/views/view_task_order_mail.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/incoming_detail_controller.dart';

class IncomingDetailView extends GetView<IncomingDetailController> {
  const IncomingDetailView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: customAppBar('Detail Surat Masukk'),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                controller.isLoading.value
                    ? const LoadingMail()
                    : _checkEmailType(),
                const SizedBox(height: 16),
                Obx(
                  () => controller.isLoading.value == false &&
                          controller.detailMail.status != 'ARCHIEVED'
                      ? MaterialButton(
                          onPressed: () {
                            showArchiveModal(context, controller.archiveMail);
                          },
                          color: const Color.fromARGB(255, 114, 72, 44),
                          child: const Text(
                            'Arsipkan',
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        )
                      : const Text(''),
                ),
                MaterialButton(
                  onPressed: () {
                    Get.back();
                  },
                  color: const Color.fromRGBO(30, 41, 60, 1),
                  child: const Text(
                    'Kembali',
                    style: TextStyle(color: Colors.white, fontSize: 14),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _checkEmailType() {
    switch (controller.mailType.value) {
      case MailAbstract.circularEmail:
        {
          return const ViewCircularMail();
        }
      case MailAbstract.taskOrderEmail:
        {
          return const ViewTaskOrderMail();
        }
      case MailAbstract.invitationEmail:
        {
          return const ViewInvitationMail();
        }
      case MailAbstract.activityParticipation:
        {
          return const ViewParticipationMail();
        }
      case MailAbstract.officialNoteEmail:
        {
          return const ViewOfficialNoteMail();
        }
      default:
        {
          return const NotFoundMail();
        }
    }
  }
}

class LoadingMail extends StatelessWidget {
  const LoadingMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Loading'),
    );
  }
}

class NotFoundMail extends StatelessWidget {
  const NotFoundMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Tipe Surat tidak Ditemukan'),
    );
  }
}
