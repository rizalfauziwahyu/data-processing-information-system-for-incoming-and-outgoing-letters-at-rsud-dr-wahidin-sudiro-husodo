import 'package:app_surat/app/interfaces/circular_mail_abstract.dart';
import 'package:app_surat/app/interfaces/mail_abstract.dart';
import 'package:app_surat/app/interfaces/response_status.dart';
import 'package:app_surat/app/methods/http_request.dart';
import 'package:app_surat/app/model/user_model.dart';
import 'package:app_surat/app/routes/app_pages.dart';
import 'package:get/get.dart';

class EditLetterController extends GetxController {
  final List<String> traits = [
    'BIASA',
    'RAHASIA',
    'SANGAT_RAHASIA',
    'KONFIDENSIAL',
  ];

  late List<User> users = [];

  final circularMail = CircularMailAbstract().obs;
  RxInt mailId = 0.obs;
  RxString mailType = ''.obs;
  RxBool isLoading = true.obs;
  RxBool loadingUser = true.obs;
  RxInt participantCount = 1.obs;

  RxMap<String, dynamic> formData = {
    'referenceNumber': '',
    'trait': null,
    'type': '',
    'status': '',
    'subject': '',
    'recipientId': null, // Ganti
    'circularMail': {'content': ''},
    'taskOrderMail': {'content': ''},
    'invitationMail': {
      'date': DateTime.now(),
      'startTime': DateTime.now(),
      'endTime': DateTime.now(),
      'place': '',
      'clothes': '',
      'content': ''
    },
    'officialNoteMails': [
      {'item': '', 'quantity': 0, 'unit': ''}
    ],
    'activityParticipationMail': {'followUpFrom': '', 'content': ''}
  }.obs;

  @override
  void onInit() async {
    super.onInit();
    mailId = Get.arguments['id'];
    mailType.value = Get.arguments['type'].toString();
    onFormChange('type', Get.arguments.toString());
    await getListUsers();
    getInitialData();
  }

  void onFormChange(key, value) {
    formData[key] = value;
  }

  void onCircularMailChange(key, value) {
    formData['circularMail'][key] = value;
  }

  void onTaskOrderMailChange(key, value) {
    formData['taskOrderMail'][key] = value;
  }

  void onInvitationMailChange(key, value) {
    formData['invitationMail'][key] = value;
  }

  void onOfficialMailChange(key, index, value) {
    formData['officialNoteMails'][index][key] = value;
  }

  void onParticipationMail(key, value) {
    formData['activityParticipationMail'][key] = value;
  }

  void addParticipation() {
    participantCount.value += 1;
    formData['officialNoteMails'].add({'item': '', 'quantity': 0, 'unit': ''});
  }

  void deleteParticipation() {
    if (participantCount.value > 1) {
      formData['officialNoteMails'].removeAt(participantCount.value - 1);
      participantCount.value -= 1;
    }
  }

  void getInitialData() async {
    try {
      ResponseStatus result = await getById('mails', mailId.value.toString());
      if (result.data != null) {
        if (mailType.value == MailAbstract.officialNoteEmail) {
          participantCount.value =
              result.data!['data']['officialNoteMails'].length;
        }
        formData.value = {
          ...result.data!['data'],
          'recipientId': result.data!['data']['recipientId'].toString(),
          "status": "PROCESS",
        };
      }
    } catch (err) {
      print(err.toString());
    } finally {
      isLoading.value = false;
    }
  }

  getListUsers() async {
    try {
      ResponseStatus response = await get('users/all');
      if (response.data != null) {
        response.data?['data'].forEach((user) {
          users.add(User.fromJson(user));
        });
      }
      loadingUser.value = false;
    } catch (err) {
      print(err.toString());
      loadingUser.value = false;
    }
  }

  void updateMail() async {
    try {
      formData.remove('id');

      Map<String, dynamic> submittedForm = {...formData};
      submittedForm['recipientId'] = int.parse(submittedForm['recipientId']);

      ResponseStatus response =
          await patch('mails', mailId.value.toString(), submittedForm);

      print(response.statusCode);
      if (response.statusCode == 200) {
        Get.offAllNamed(Routes.DASHBOARD);
      }
    } catch (err) {
      print(err.toString());
    }
  }
}
