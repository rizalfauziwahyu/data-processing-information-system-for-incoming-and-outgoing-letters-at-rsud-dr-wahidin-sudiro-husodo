import 'package:app_surat/app/components/app_bar.dart';
import 'package:app_surat/app/interfaces/mail_abstract.dart';
import 'package:app_surat/app/modules/edit_letter/views/activity_participation_mail.dart';
import 'package:app_surat/app/modules/edit_letter/views/circular_mail.dart';
import 'package:app_surat/app/modules/edit_letter/views/invitation_mail.dart';
import 'package:app_surat/app/modules/edit_letter/views/official_note_mail.dart';
import 'package:app_surat/app/modules/edit_letter/views/task_order_mail.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/edit_letter_controller.dart';

class EditLetterView extends GetView<EditLetterController> {
  const EditLetterView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: customAppBar('Tulis Surat'),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
        child: SingleChildScrollView(
          child: _checkEmailType(),
        ),
      ),
    );
  }

  _checkEmailType() {
    switch (controller.mailType.value) {
      case MailAbstract.circularEmail:
        {
          return EditCircularMail();
        }
      case MailAbstract.taskOrderEmail:
        {
          return EditTaskOrderMail();
        }
      case MailAbstract.invitationEmail:
        {
          return EditInvitationMail();
        }
      case MailAbstract.activityParticipation:
        {
          return EditActivityParticipationMail();
        }
      case MailAbstract.officialNoteEmail:
        {
          return EditOfficialNoteMail();
        }
      default:
        {
          return const NotFoundMail();
        }
    }
  }
}

class NotFoundMail extends StatelessWidget {
  const NotFoundMail({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Tipe Surat tidak Ditemukan'),
    );
  }
}
