import 'package:app_surat/app/modules/edit_letter/controllers/edit_letter_controller.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EditCircularMail extends GetView<EditLetterController> {
  final _formKey = GlobalKey<FormState>();

  final double verticalGap = 20.0;
  final double horizontalGap = 8.0;
  final double childVerticalGap = 6;

  EditCircularMail({super.key});

  @override
  Widget build(BuildContext context) {
    double cWidth = MediaQuery.of(context).size.width * 0.6;
    return Obx(
      () => controller.isLoading.value
          ? const Center(
              child: Text('Loading'),
            )
          : Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const Center(
                    child: Text(
                      'Surat Edaran',
                      style: TextStyle(
                          color: Color.fromRGBO(30, 41, 60, 1),
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Container(
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(224, 227, 227, 1),
                        borderRadius: BorderRadius.all(Radius.circular(2))),
                    padding: const EdgeInsets.all(13),
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.brown.shade800,
                          child: const Text('AH'),
                        ),
                        SizedBox(width: horizontalGap),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            const Text(
                              'Pengirim / Asal Surat',
                              style: TextStyle(
                                fontSize: 12,
                                color: Color.fromRGBO(30, 41, 60, 1),
                              ),
                            ),
                            const SizedBox(height: 4),
                            SizedBox(
                              width: cWidth,
                              child: const Text(
                                'Bagian Umum RSUD',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Color.fromRGBO(30, 41, 60, 1),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: verticalGap),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Nomor Surat',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                      const SizedBox(height: 6),
                      TextFormField(
                        initialValue: controller.formData['referenceNumber'],
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (GetUtils.isNullOrBlank(value!) == false) {
                            return null;
                          }
                          return "Enter Text";
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10.0),
                          hintText: 'Tulis Nomor Surat',
                          hintStyle: const TextStyle(
                            fontSize: 12,
                            color: Color.fromRGBO(30, 41, 60, 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                        onChanged: ((value) {
                          controller.onFormChange('referenceNumber', value);
                        }),
                      ),
                    ],
                  ),
                  SizedBox(height: verticalGap),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Subject',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                      const SizedBox(height: 6),
                      TextFormField(
                        initialValue: controller.formData['subject'],
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (GetUtils.isNullOrBlank(value!) == false) {
                            return null;
                          }
                          return "Enter Text";
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10.0),
                          hintText: 'Tulis Subject',
                          hintStyle: const TextStyle(
                            fontSize: 12,
                            color: Color.fromRGBO(30, 41, 60, 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                        onChanged: ((value) {
                          controller.onFormChange('subject', value);
                        }),
                      ),
                    ],
                  ),
                  SizedBox(height: verticalGap),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'Sifat Surat',
                              style: TextStyle(
                                fontSize: 12,
                                color: Color.fromRGBO(30, 41, 60, 1),
                              ),
                            ),
                            DropdownButtonHideUnderline(
                              child: Obx(
                                () => DropdownButtonFormField2(
                                  isExpanded: true,
                                  validator: (value) {
                                    if (value == null) {
                                      return 'Sifat Surat is required';
                                    }
                                    return null;
                                  },
                                  hint: Text(
                                    'Pilih Sifat Surat',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Theme.of(context).hintColor,
                                    ),
                                  ),
                                  buttonStyleData: ButtonStyleData(
                                    height: 50,
                                    width: 160,
                                    padding: const EdgeInsets.only(
                                        left: 14, right: 14),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                          childVerticalGap),
                                      border: Border.all(
                                        color: Colors.black26,
                                      ),
                                      color: Colors.white,
                                    ),
                                    elevation: 2,
                                  ),
                                  items: controller.traits
                                      .map(
                                        (item) => DropdownMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                            style: const TextStyle(
                                              fontSize: 14,
                                            ),
                                          ),
                                        ),
                                      )
                                      .toList(),
                                  value: controller.formData['trait'],
                                  onChanged: (value) {
                                    controller.onFormChange('trait', value);
                                  },
                                  menuItemStyleData: const MenuItemStyleData(
                                    height: 40,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 10),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'User Id',
                              style: TextStyle(
                                fontSize: 12,
                                color: Color.fromRGBO(30, 41, 60, 1),
                              ),
                            ),
                            const SizedBox(height: 6),
                            Obx(
                              () => controller.loadingUser.value
                                  ? const Text('Loading User')
                                  : DropdownButtonHideUnderline(
                                      child: Obx(
                                        () => DropdownButtonFormField2(
                                          isExpanded: true,
                                          validator: (value) {
                                            if (value == null) {
                                              return 'Sifat Surat is required';
                                            }
                                            return null;
                                          },
                                          hint: Text(
                                            'Pilih Sifat Surat',
                                            style: TextStyle(
                                              fontSize: 14,
                                              color:
                                                  Theme.of(context).hintColor,
                                            ),
                                          ),
                                          buttonStyleData: ButtonStyleData(
                                            height: 50,
                                            width: 160,
                                            padding: const EdgeInsets.only(
                                                left: 14, right: 14),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      childVerticalGap),
                                              border: Border.all(
                                                color: Colors.black26,
                                              ),
                                              color: Colors.white,
                                            ),
                                            elevation: 2,
                                          ),
                                          items: controller.users
                                              .map(
                                                (item) =>
                                                    DropdownMenuItem<String>(
                                                  value: item.id.toString(),
                                                  child: Text(
                                                    item.name as String,
                                                    style: const TextStyle(
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                ),
                                              )
                                              .toList(),
                                          value: controller
                                              .formData['recipientId'],
                                          onChanged: (value) {
                                            controller.onFormChange(
                                                'recipientId', value);
                                          },
                                          menuItemStyleData:
                                              const MenuItemStyleData(
                                            height: 40,
                                          ),
                                        ),
                                      ),
                                    ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: verticalGap),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Acara',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color.fromRGBO(30, 41, 60, 1),
                        ),
                      ),
                      const SizedBox(height: 6),
                      TextFormField(
                        initialValue: controller.formData['circularMail']
                            ['content'],
                        validator: (value) {
                          if (GetUtils.isNullOrBlank(value!) == false) {
                            return null;
                          }
                          return "Enter Text";
                        },
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(10.0),
                          hintText: 'Tulis Acara',
                          hintStyle: const TextStyle(
                            fontSize: 12,
                            color: Color.fromRGBO(30, 41, 60, 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(30, 41, 60, 1)),
                            borderRadius: BorderRadius.circular(3),
                          ),
                        ),
                        minLines:
                            6, // any number you need (It works as the rows for the textarea)
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        onChanged: (value) {
                          controller.onCircularMailChange('content', value);
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: verticalGap),
                  Obx(
                    () => MaterialButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          controller.updateMail();
                        }
                      },
                      color: const Color.fromRGBO(30, 41, 60, 1),
                      child: controller.isLoading.value
                          ? const SizedBox(
                              height: 10.0,
                              width: 10.0,
                              child: CircularProgressIndicator(
                                color: Colors.white,
                              ),
                            )
                          : const Text(
                              'Verifikasikan Surat',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                    ),
                  ),
                  SizedBox(height: verticalGap),
                ],
              ),
            ),
    );
  }
}
