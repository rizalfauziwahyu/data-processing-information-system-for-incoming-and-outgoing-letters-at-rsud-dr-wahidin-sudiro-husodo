import 'package:get/get.dart';

import '../controllers/edit_letter_controller.dart';

class EditLetterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EditLetterController>(
      () => EditLetterController(),
    );
  }
}
