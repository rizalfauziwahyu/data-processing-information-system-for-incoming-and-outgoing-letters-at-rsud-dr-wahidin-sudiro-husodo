import 'package:intl/intl.dart';

class Mails {
  List<Mail>? data;

  Mails({this.data});

  Mails.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Mail>[];
      json['data'].forEach((v) {
        data!.add(Mail.fromJson(v));
      });
    }
  }
}

class AdminMail {
  int? id;
  String? referenceNumber;
  String? trait;
  String? subject;
  String? status;
  String? type;
  String? revision;
  String? templateId;
  String? createdAt;
  int? fromUserId;
  int? recipientId;

  AdminMail({
    this.id,
    this.referenceNumber,
    this.trait,
    this.subject,
    this.status,
    this.type,
    this.revision,
    this.templateId,
    this.createdAt,
    this.fromUserId,
    this.recipientId,
  });

  AdminMail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    referenceNumber = json['referenceNumber'];
    trait = json['trait'];
    subject = json['subject'];
    status = json['status'];
    type = json['type'];
    createdAt = json['createdAt'];
    revision = json['revision'];
    templateId = json['templateId'];
    fromUserId = json['fromUserId'];
    recipientId = json['recipientId'];
  }
}

class Mail {
  int? id;
  int? mailId;
  int? fromUserId;
  int? recipientId;
  DetailMail? mail;

  Mail({this.id, this.mailId, this.fromUserId, this.recipientId, this.mail});

  Mail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mailId = json['mailId'];
    fromUserId = json['fromUserId'];
    recipientId = json['recipientId'];
    mail = DetailMail.fromJson(json['mail']);
  }
}

class DetailMail {
  int? id;
  String? referenceNumber;
  String? trait;
  String? subject;
  String? status;
  String? type;
  String? recipient;
  String? from;
  String? createdAt;
  TaskOrderMail? taskOrderMail;
  CircularMail? circularMail;
  InvitationMail? invitationMail;
  ActivityParticipationMail? activityParticipationMail;
  List<OfficialNoteMail>? officialNoteMails;

  DetailMail(
      {this.id,
      this.referenceNumber,
      this.trait,
      this.subject,
      this.status,
      this.type,
      this.createdAt});

  DetailMail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    referenceNumber = json['referenceNumber'];
    trait = json['trait'];
    subject = json['subject'];
    status = json['status'];
    type = json['type'];
    createdAt = json['createdAt'];
    if (json['recipient'] != null) {
      recipient = json['recipient']['name'];
    }
    if (json['from'] != null) {
      from = json['from']['name'];
    }
    if (json['taskOrderMail'] != null) {
      taskOrderMail = TaskOrderMail.fromJson(json['taskOrderMail']);
    }
    if (json['circularMail'] != null) {
      circularMail = CircularMail.fromJson(json['circularMail']);
    }
    if (json['invitationMail'] != null) {
      invitationMail = InvitationMail.fromJson(json['invitationMail']);
    }
    if (json['activityParticipationMail'] != null) {
      activityParticipationMail =
          ActivityParticipationMail.fromJson(json['activityParticipationMail']);
    }
    if (json['officialNoteMails'] != null) {
      officialNoteMails = <OfficialNoteMail>[];
      json['officialNoteMails'].forEach((v) {
        officialNoteMails!.add(OfficialNoteMail.fromJson(v));
      });
    }
  }
}

class TaskOrderMail {
  String? content;

  TaskOrderMail({this.content});

  TaskOrderMail.fromJson(Map<String, dynamic> json) {
    content = json['content'];
  }
}

class CircularMail {
  String? content;

  CircularMail({this.content});

  CircularMail.fromJson(Map<String, dynamic> json) {
    content = json['content'];
  }
}

class InvitationMail {
  String? content;
  String? date;
  String? place;
  String? clothes;

  InvitationMail({this.content, this.date, this.place, this.clothes});

  InvitationMail.fromJson(Map<String, dynamic> json) {
    content = json['content'];
    place = json['place'];
    clothes = json['clothes'];
    final f = DateFormat('dd MMMM yyyy');
    date = f.format(DateTime.parse(json['date']));
  }
}

class OfficialNoteMail {
  String? item;
  int? quantity;
  String? unit;

  OfficialNoteMail({this.item, this.quantity, this.unit});

  OfficialNoteMail.fromJson(Map<String, dynamic> json) {
    item = json['item'];
    quantity = json['quantity'];
    unit = json['unit'];
  }
}

class ActivityParticipationMail {
  String? followUpFrom;
  String? content;

  ActivityParticipationMail({this.followUpFrom, this.content});

  ActivityParticipationMail.fromJson(Map<String, dynamic> json) {
    followUpFrom = json['followUpFrom'];
    content = json['content'];
  }
}
