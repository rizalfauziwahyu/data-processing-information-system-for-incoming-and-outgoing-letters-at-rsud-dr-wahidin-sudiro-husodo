class User {
  int? id;
  String? name;
  String? username;
  String? profilePhoto;
  String? role;

  User({this.id, this.name, this.profilePhoto, this.role});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    profilePhoto = json['profilePhoto'];
    role = json['role'];
  }
}
