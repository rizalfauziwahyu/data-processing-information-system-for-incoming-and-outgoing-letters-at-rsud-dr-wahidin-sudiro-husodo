import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../global/api_config.dart' as globals;
import '../routes/app_pages.dart';

PreferredSizeWidget customAppBar(String title) {
  return AppBar(
    title: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(title),
        MaterialButton(
          onPressed: () {
            logout();
          },
          color: Colors.white,
          child: const Text(
            'Logout',
            style:
                TextStyle(color: Color.fromRGBO(30, 41, 60, 1), fontSize: 14),
          ),
        ),
      ],
    ),
    backgroundColor: const Color.fromRGBO(30, 41, 60, 1),
  );
}

void logout() {
  globals.user = null;
  globals.token = "";
  Get.offAllNamed(Routes.LOGIN);
}
