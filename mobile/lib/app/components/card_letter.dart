import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

Widget cardLetter(BuildContext context, String subject, String referenceNumber,
    String trait, String date, String status, Function onClick) {
  return GestureDetector(
    onTap: () {
      onClick();
    },
    child: Container(
      decoration: const BoxDecoration(
          color: Color.fromRGBO(233, 233, 233, 1),
          borderRadius: BorderRadius.all(Radius.circular(4))),
      padding: const EdgeInsets.all(8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 24,
                backgroundColor: Colors.brown.shade800,
                child: const Text('AH'),
              ),
              const SizedBox(width: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(height: 4),
                  SizedBox(
                    width: 150,
                    child: Text(
                      subject,
                      style: const TextStyle(
                        fontSize: 14,
                        color: Color.fromRGBO(30, 41, 60, 1),
                      ),
                    ),
                  ),
                  const SizedBox(height: 4),
                  Text(
                    referenceNumber,
                    softWrap: false,
                    maxLines: 5,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      fontSize: 10,
                      color: Color.fromRGBO(30, 41, 60, 1),
                    ),
                  ),
                  const SizedBox(height: 4),
                  Container(
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(55, 55, 55, 1),
                        borderRadius: BorderRadius.all(Radius.circular(4))),
                    padding: const EdgeInsets.all(4),
                    child: Text(
                      trait,
                      style: const TextStyle(
                        fontSize: 10,
                        color: Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              const SizedBox(height: 4),
              SizedBox(
                child: Text(
                  DateFormat('dd MMMM yyyy').format(DateTime.parse(date)),
                  style: const TextStyle(
                    fontSize: 14,
                    color: Color.fromRGBO(30, 41, 60, 1),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Container(
                decoration: const BoxDecoration(
                    color: Color.fromRGBO(175, 172, 172, 1),
                    borderRadius: BorderRadius.all(Radius.circular(4))),
                padding: const EdgeInsets.all(4),
                child: Text(
                  status,
                  style: const TextStyle(
                    fontSize: 10,
                    color: Color.fromRGBO(26, 25, 25, 1),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    ),
  );
}
