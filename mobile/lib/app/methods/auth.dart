import 'dart:convert';

import 'package:app_surat/app/model/user_model.dart';
import 'package:http/http.dart' as http;

import '../global/api_config.dart' as globals;
import '../interfaces/response_status.dart';

Future<ResponseStatus> login(
    String username, String password, String ip) async {
  String loginUrl = '${ip}api/auth/signin';
  Map<String, dynamic> body = {"username": username, "password": password};

  http.Response response = await http.post(Uri.parse(loginUrl), body: body);
  Map<String, dynamic> result = jsonDecode(response.body);

  print(result.toString());

  if (response.statusCode == 200) {
    globals.user = User.fromJson(result['data']);
    globals.token = result['data']['access_token'];
  }

  String message = result['message'];
  Map<String, dynamic> data;
  if (result['data'] != null) {
    data = result['data'];
  } else {
    data = {};
  }
  return ResponseStatus(
      statusCode: response.statusCode, message: message, data: data);
}

Future<ResponseStatus> register(String username, String password, String name,
    String role, String ip) async {
  String loginUrl = '${ip}api/auth/signup';
  Map<String, dynamic> body = {
    "username": username,
    "password": password,
    "name": name,
    "role": role
  };

  http.Response response = await http.post(Uri.parse(loginUrl), body: body);
  Map<String, dynamic> result = jsonDecode(response.body);

  if (response.statusCode == 200) {
    globals.user = User.fromJson(result['data']);
    globals.token = result['data']['access_token'];
  }
  Map<String, dynamic> data;
  if (result['data'] != null) {
    data = result['data'];
  } else {
    data = {};
  }

  String message = result['message'];
  return ResponseStatus(
      statusCode: response.statusCode, message: message, data: data);
}
