import 'dart:convert';

import 'package:app_surat/app/global/api_config.dart' as globals;
import 'package:app_surat/app/interfaces/response_status.dart';
import 'package:http/http.dart' as http;

Future<ResponseStatus> post(String route, Map<String, dynamic> body) async {
  String url = '${globals.url}api/$route';

  http.Response response = await http.post(
    Uri.parse(url),
    headers: {
      "content-type": 'application/json',
      "Authorization": 'Bearer ${globals.token}',
    },
    body: json.encode(body, toEncodable: myEncode),
  );
  Map<String, dynamic> result = jsonDecode(response.body);
  print(result.toString());
  String message = result['message'];

  return ResponseStatus(
      statusCode: response.statusCode, message: message, data: result);
}

Future<ResponseStatus> get(String route) async {
  String url = '${globals.url}api/$route';

  http.Response response = await http.get(
    Uri.parse(url),
    headers: {
      "Authorization": 'Bearer ${globals.token}',
    },
  );
  Map<String, dynamic> result = jsonDecode(response.body);
  String message = result['message'];

  return ResponseStatus(
      statusCode: response.statusCode, message: message, data: result);
}

Future<ResponseStatus> getById(String route, String id) async {
  String url = '${globals.url}api/$route/$id';

  http.Response response = await http.get(
    Uri.parse(url),
    headers: {
      "Authorization": 'Bearer ${globals.token}',
    },
  );
  Map<String, dynamic> result = jsonDecode(response.body);
  String message = result['message'];

  return ResponseStatus(
      statusCode: response.statusCode, message: message, data: result);
}

Future<ResponseStatus> patch(
    String route, String id, Map<String, dynamic> body) async {
  String url = '${globals.url}api/$route/$id';

  http.Response response = await http.patch(
    Uri.parse(url),
    headers: {
      "content-type": 'application/json',
      "Authorization": 'Bearer ${globals.token}',
    },
    body: json.encode(body, toEncodable: myEncode),
  );
  Map<String, dynamic> result = jsonDecode(response.body);
  String message = result['message'];

  return ResponseStatus(
      statusCode: response.statusCode, message: message, data: result);
}

dynamic myEncode(dynamic item) {
  if (item is DateTime) {
    return item.toIso8601String();
  }
  return item;
}
