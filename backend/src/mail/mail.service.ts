import {
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { Mail, MailStatus } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import {
  ArchieveMailDto,
  CreateMailDto,
  MailType,
  UpdateMailDto,
} from './dto';

@Injectable()
export class MailService {
  constructor(private prisma: PrismaService) {}

  async getArchievedMail(userId: number) {
    try {
      const draftMails =
        await this.prisma.mail.findMany({
          where: {
            status: MailStatus.ARCHIEVED,
            fromUserId: userId,
          },
          include: {
            from: {
              select: {
                name: true,
              },
            },
            recipient: {
              select: {
                name: true,
              },
            },
          },
        });

      return draftMails;
    } catch (err) {
      throw err;
    }
  }

  async getArchievedInboxMail(userId: number) {
    try {
      const draftMails =
        await this.prisma.mail.findMany({
          where: {
            status: MailStatus.ARCHIEVED,
            recipientId: userId,
          },
          include: {
            from: {
              select: {
                name: true,
              },
            },
            recipient: {
              select: {
                name: true,
              },
            },
          },
        });

      return draftMails;
    } catch (err) {
      throw err;
    }
  }

  async archieveMail(
    userId: number,
    dto: ArchieveMailDto,
  ) {
    try {
      const mailBox =
        await this.prisma.mailBox.findUnique({
          where: {
            id: dto.mailBoxId,
          },
        });

      if (!mailBox)
        throw new NotFoundException(
          'this resource doesnt exist',
        );

      await this.prisma.mailArchive.create({
        data: {
          isArchieved: true,
          mailBoxId: dto.mailBoxId,
          userId: userId,
        },
      });

      return await this.prisma.mail.update({
        where: {
          id: mailBox.mailId,
        },
        data: {
          status: MailStatus.ARCHIEVED,
        },
      });
    } catch (err) {
      throw err;
    }
  }

  async getStatisticMail(userId: number) {
    try {
      const JanInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-01-30'),
              gte: new Date('2023-01-01'),
            },
          },
        });
      const FebInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-02-30'),
              gte: new Date('2023-02-01'),
            },
          },
        });
      const MarInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-03-30'),
              gte: new Date('2023-03-01'),
            },
          },
        });
      const AprInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-04-30'),
              gte: new Date('2023-04-01'),
            },
          },
        });
      const MeiInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-05-30'),
              gte: new Date('2023-05-01'),
            },
          },
        });
      const JunInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-06-30'),
              gte: new Date('2023-06-01'),
            },
          },
        });
      const JulInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-07-30'),
              gte: new Date('2023-07-01'),
            },
          },
        });
      const AguInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-08-30'),
              gte: new Date('2023-08-01'),
            },
          },
        });
      const SepInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-09-30'),
              gte: new Date('2023-09-01'),
            },
          },
        });
      const OktInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-10-30'),
              gte: new Date('2023-10-01'),
            },
          },
        });
      const NovInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-11-30'),
              gte: new Date('2023-11-01'),
            },
          },
        });
      const DesInbox =
        await this.prisma.mailBox.count({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-12-30'),
              gte: new Date('2023-12-01'),
            },
          },
        });

      const JanSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-01-30'),
              gte: new Date('2023-01-01'),
            },
          },
        });
      const FebSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-02-30'),
              gte: new Date('2023-02-01'),
            },
          },
        });
      const MarSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-03-30'),
              gte: new Date('2023-03-01'),
            },
          },
        });
      const AprSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-04-30'),
              gte: new Date('2023-04-01'),
            },
          },
        });
      const MeiSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-05-30'),
              gte: new Date('2023-05-01'),
            },
          },
        });
      const JunSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-06-30'),
              gte: new Date('2023-06-01'),
            },
          },
        });
      const JulSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-07-30'),
              gte: new Date('2023-07-01'),
            },
          },
        });
      const AguSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-08-30'),
              gte: new Date('2023-08-01'),
            },
          },
        });
      const SepSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-09-30'),
              gte: new Date('2023-09-01'),
            },
          },
        });
      const OktSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-10-30'),
              gte: new Date('2023-10-01'),
            },
          },
        });
      const NovSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-11-30'),
              gte: new Date('2023-11-01'),
            },
          },
        });
      const DesSent =
        await this.prisma.mailBox.count({
          where: {
            fromUserId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
            createdAt: {
              lte: new Date('2023-12-30'),
              gte: new Date('2023-12-01'),
            },
          },
        });

      return [
        {
          id: 1,
          month: 'Jan',
          surat_masuk: JanInbox,
          surat_keluar: JanSent,
        },
        {
          id: 2,
          month: 'Feb',
          surat_masuk: FebInbox,
          surat_keluar: FebSent,
        },
        {
          id: 3,
          month: 'Mar',
          surat_masuk: MarInbox,
          surat_keluar: MarSent,
        },
        {
          id: 4,
          month: 'Apr',
          surat_masuk: AprInbox,
          surat_keluar: AprSent,
        },
        {
          id: 5,
          month: 'Mei',
          surat_masuk: MeiInbox,
          surat_keluar: MeiSent,
        },
        {
          id: 6,
          month: 'Jun',
          surat_masuk: JunInbox,
          surat_keluar: JunSent,
        },
        {
          id: 7,
          month: 'Jul',
          surat_masuk: JulInbox,
          surat_keluar: JulSent,
        },
        {
          id: 8,
          month: 'Agu',
          surat_masuk: AguInbox,
          surat_keluar: AguSent,
        },
        {
          id: 9,
          month: 'Sep',
          surat_masuk: SepInbox,
          surat_keluar: SepSent,
        },
        {
          id: 10,
          month: 'Okt',
          surat_masuk: OktInbox,
          surat_keluar: OktSent,
        },
        {
          id: 11,
          month: 'Nov',
          surat_masuk: NovInbox,
          surat_keluar: NovSent,
        },
        {
          id: 12,
          month: 'Des',
          surat_masuk: DesInbox,
          surat_keluar: DesSent,
        },
      ];
    } catch (err) {
      throw err;
    }
  }

  async getProcessMail() {
    try {
      const draftMails =
        await this.prisma.mail.findMany({
          where: {
            status: MailStatus.PROCESS,
          },
          include: {
            from: {
              select: {
                name: true,
              },
            },
            recipient: {
              select: {
                name: true,
              },
            },
          },
        });

      return draftMails;
    } catch (err) {
      throw err;
    }
  }

  async getDraftMail(userId: number) {
    try {
      const draftMails =
        await this.prisma.mail.findMany({
          where: {
            fromUserId: userId,
            status: MailStatus.REVISION,
          },
          include: {
            from: {
              select: {
                name: true,
              },
            },
            recipient: {
              select: {
                name: true,
              },
            },
          },
        });

      return draftMails;
    } catch (err) {
      throw err;
    }
  }

  async getMailInboxes(userId: number) {
    try {
      const mailInboxes =
        await this.prisma.mailBox.findMany({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
          },
          include: {
            mail: true,
            from: {
              select: {
                name: true,
              },
            },
            recipient: {
              select: {
                name: true,
              },
            },
          },
        });

      return mailInboxes;
    } catch (err) {
      throw err;
    }
  }

  async getMailAgenda(userId: number) {
    try {
      const mailInboxes =
        await this.prisma.mailBox.findMany({
          where: {
            recipientId: userId,
            mail: {
              status: MailStatus.VERIFIED,
            },
          },
          include: {
            mail: true,
            from: {
              select: {
                name: true,
              },
            },
            recipient: {
              select: {
                name: true,
              },
            },
          },
        });

      const mailAgendas = [];

      mailInboxes.map((mailAgenda) => {
        mailAgendas.push({
          start: mailAgenda.createdAt,
          end: mailAgenda.createdAt,
          title: mailAgenda.mail.subject,
        });
      });

      return mailAgendas;
    } catch (err) {
      throw err;
    }
  }

  async getMailSents(userId: number) {
    try {
      const mailSents =
        await this.prisma.mailBox.findMany({
          where: {
            fromUserId: userId,
          },
          include: {
            mail: true,
            from: {
              select: {
                name: true,
              },
            },
            recipient: {
              select: {
                name: true,
              },
            },
          },
        });

      return mailSents;
    } catch (err) {
      throw err;
    }
  }

  async getMailDetail(
    mailId: number,
  ): Promise<Mail> {
    try {
      const mail =
        await this.prisma.mail.findUnique({
          where: {
            id: mailId,
          },
          include: {
            recipient: {
              select: {
                name: true,
              },
            },
            from: {
              select: {
                name: true,
              },
            },
            officialNoteMails: true,
            taskOrderMail: true,
            circularMail: true,
            invitationMail: true,
            activityParticipationMail: true,
          },
        });

      return mail;
    } catch (err) {
      throw err;
    }
  }

  async saveAsDraft(
    userId: number,
    dto: CreateMailDto,
  ): Promise<Mail> {
    try {
      return await this.prisma.$transaction(
        async (tx) => {
          const mail = await tx.mail.create({
            data: {
              referenceNumber:
                dto.reference_number,
              trait: dto.trait,
              status: dto.status,
              subject: dto.subject,
              type: dto.type,
              fromUserId: userId,
              recipientId: dto.recipientId,
              date: dto.date,
            },
          });

          if (
            dto.type ==
            MailType.ACTIVITYPARTICIPATION
          ) {
            await tx.activityParticipationMail.create(
              {
                data: {
                  followUpFrom:
                    dto.activityParticipationMail
                      .followUpFrom,
                  content:
                    dto.activityParticipationMail
                      .content,
                  fileAttachment:
                    dto.activityParticipationMail
                      .fileAttachment,
                  mailId: mail.id,
                },
              },
            );
          } else if (
            dto.type == MailType.CIRCULARMAIL
          ) {
            await tx.circularMail.create({
              data: {
                content: dto.circularMail.content,
                mailId: mail.id,
              },
            });
          } else if (
            dto.type == MailType.INVITATIONMAIL
          ) {
            await tx.invitationMail.create({
              data: {
                date: new Date(
                  dto.invitationMail.date,
                ),
                startTime: new Date(
                  dto.invitationMail.startTime,
                ),
                endTime: new Date(
                  dto.invitationMail.endTime,
                ),
                place: dto.invitationMail.place,
                clothes:
                  dto.invitationMail.clothes,
                content:
                  dto.invitationMail.content,
                mailId: mail.id,
              },
            });
          } else if (
            dto.type == MailType.TASKORDERMAIL
          ) {
            await tx.taskOrderMail.create({
              data: {
                content:
                  dto.taskOrderMail.content,
                fileAttachment:
                  dto.taskOrderMail
                    .fileAttachment,
                mailId: mail.id,
              },
            });
          } else if (
            dto.type == MailType.OFFICIALNOTEMAIL
          ) {
            await tx.officialNoteMail.createMany({
              data: dto.officialNoteMail.map(
                (item) => {
                  return {
                    ...item,
                    mailId: mail.id,
                  };
                },
              ),
            });
          }

          return mail;
        },
      );
    } catch (err) {
      throw err;
    }
  }

  async sent(userId: number, dto: CreateMailDto) {
    try {
      const mail = await this.saveAsDraft(
        userId,
        dto,
      );

      await this.prisma.mail.update({
        data: {
          status: MailStatus.PROCESS,
          mailBox: {
            create: {
              from: {
                connect: {
                  id: userId,
                },
              },
              recipient: {
                connect: {
                  id: mail.recipientId,
                },
              },
            },
          },
        },
        where: {
          id: mail.id,
        },
      });

      return {
        message: 'Email berhasil dikirim!',
      };
    } catch (err) {
      throw err;
    }
  }

  async update(
    mailId: number,
    dto: UpdateMailDto,
  ) {
    try {
      const mail =
        await this.prisma.mail.findUnique({
          where: {
            id: mailId,
          },
        });

      if (!mail)
        throw new NotFoundException(
          'this resource does not exists',
        );

      return await this.prisma.$transaction(
        async (tx) => {
          const mail = await tx.mail.update({
            where: {
              id: mailId,
            },
            data: {
              status: dto.status,
              referenceNumber:
                dto.reference_number,
              trait: dto.trait,
              subject: dto.subject,
              revision: dto.revision,
              recipientId: dto.recipientId,
            },
          });

          if (
            dto.type ==
            MailType.ACTIVITYPARTICIPATION
          ) {
            await tx.activityParticipationMail.update(
              {
                where: {
                  mailId: mail.id,
                },
                data: {
                  followUpFrom:
                    dto.activityParticipationMail
                      .followUpFrom,
                  content:
                    dto.activityParticipationMail
                      .content,
                  fileAttachment:
                    dto.activityParticipationMail
                      .fileAttachment,
                  mailId: mail.id,
                },
              },
            );
          } else if (
            dto.type == MailType.CIRCULARMAIL
          ) {
            await tx.circularMail.update({
              where: {
                mailId: mail.id,
              },
              data: {
                content: dto.circularMail.content,
                mailId: mail.id,
              },
            });
          } else if (
            dto.type == MailType.INVITATIONMAIL
          ) {
            await tx.invitationMail.update({
              where: {
                mailId: mail.id,
              },
              data: {
                date: new Date(
                  dto.invitationMail.date,
                ),
                startTime: new Date(
                  dto.invitationMail.startTime,
                ),
                endTime: new Date(
                  dto.invitationMail.endTime,
                ),
                place: dto.invitationMail.place,
                clothes:
                  dto.invitationMail.clothes,
                content:
                  dto.invitationMail.content,
                mailId: mail.id,
              },
            });
          } else if (
            dto.type == MailType.TASKORDERMAIL
          ) {
            await tx.taskOrderMail.update({
              where: {
                mailId: mail.id,
              },
              data: {
                content:
                  dto.taskOrderMail.content,
                fileAttachment:
                  dto.taskOrderMail
                    .fileAttachment,
                mailId: mail.id,
              },
            });
          } else if (
            dto.type == MailType.OFFICIALNOTEMAIL
          ) {
            await tx.officialNoteMail.deleteMany({
              where: {
                mailId: mail.id,
              },
            });

            await tx.officialNoteMail.createMany({
              data: dto.officialNoteMail.map(
                (item) => {
                  return {
                    ...item,
                    mailId: mail.id,
                  };
                },
              ),
            });
          }

          return mail;
        },
      );
    } catch (err) {
      throw err;
    }
  }
}
