import {
  Controller,
  UseGuards,
  Param,
  Patch,
  Post,
  Get,
  Delete,
  Body,
  ParseIntPipe,
} from '@nestjs/common';
import { MailService } from './mail.service';
import { JwtGuard } from 'src/auth/guard';
import { GetUser } from 'src/auth/decorator';
import {
  ArchieveMailDto,
  CreateMailDto,
  UpdateMailDto,
} from './dto';

@UseGuards(JwtGuard)
@Controller('api/mails')
export class MailController {
  constructor(private mailService: MailService) {}

  @Get('archieved')
  getArchievedMail(
    @GetUser('id') userId: number,
  ) {
    return this.mailService.getArchievedMail(
      userId,
    );
  }

  @Get('agenda')
  getMailAgenda(@GetUser('id') userId: number) {
    return this.mailService.getMailAgenda(userId);
  }

  @Get('inbox-archieved')
  getArchievedInboxMail(
    @GetUser('id') userId: number,
  ) {
    return this.mailService.getArchievedInboxMail(
      userId,
    );
  }

  @Get('statistic')
  getMailStatistic(
    @GetUser('id') userId: number,
  ) {
    return this.mailService.getStatisticMail(
      userId,
    );
  }

  @Get()
  getDraftMail(@GetUser('id') userId: number) {
    return this.mailService.getDraftMail(userId);
  }

  @Get('inboxes')
  getMailInboxes(@GetUser('id') userId: number) {
    return this.mailService.getMailInboxes(
      userId,
    );
  }

  @Get('process')
  getProcessMail() {
    return this.mailService.getProcessMail();
  }

  @Get('sents')
  getMailSents(@GetUser('id') userId: number) {
    return this.mailService.getMailSents(userId);
  }

  @Post('draft')
  saveAsDraft(
    @GetUser('id') userId: number,
    @Body() dto: CreateMailDto,
  ) {
    return this.mailService.saveAsDraft(
      userId,
      dto,
    );
  }

  @Post('archieve-mail')
  archieveMail(
    @GetUser('id') userId: number,
    @Body() dto: ArchieveMailDto,
  ) {
    return this.mailService.archieveMail(
      userId,
      dto,
    );
  }

  @Post('sent')
  sent(
    @GetUser('id') userId: number,
    @Body() dto: CreateMailDto,
  ) {
    return this.mailService.sent(userId, dto);
  }

  @Get(':id')
  getMailDetail(
    @GetUser('id') userId: number,
    @Param('id', ParseIntPipe) mailId: number,
  ) {
    return this.mailService.getMailDetail(mailId);
  }

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) mailId: number,
    @Body() dto: UpdateMailDto,
  ) {
    return this.mailService.update(mailId, dto);
  }
}
