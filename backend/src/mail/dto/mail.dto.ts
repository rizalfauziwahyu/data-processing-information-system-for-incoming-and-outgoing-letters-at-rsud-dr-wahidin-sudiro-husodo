import {
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export const MailStatus: {
  PENDING: 'PENDING';
  VERIFIED: 'VERIFIED';
  REVISION: 'REVISION';
  PROCESS: 'PROCESS';
  DRAFT: 'DRAFT';
} = {
  PENDING: 'PENDING',
  VERIFIED: 'VERIFIED',
  REVISION: 'REVISION',
  PROCESS: 'PROCESS',
  DRAFT: 'DRAFT',
};

export const MailType: {
  OFFICIALNOTEMAIL: 'OFFICIALNOTEMAIL';
  TASKORDERMAIL: 'TASKORDERMAIL';
  INVITATIONMAIL: 'INVITATIONMAIL';
  CIRCULARMAIL: 'CIRCULARMAIL';
  ACTIVITYPARTICIPATION: 'ACTIVITYPARTICIPATION';
} = {
  OFFICIALNOTEMAIL: 'OFFICIALNOTEMAIL',
  TASKORDERMAIL: 'TASKORDERMAIL',
  INVITATIONMAIL: 'INVITATIONMAIL',
  CIRCULARMAIL: 'CIRCULARMAIL',
  ACTIVITYPARTICIPATION: 'ACTIVITYPARTICIPATION',
};

export const Trait: {
  BIASA: 'BIASA';
  RAHASIA: 'RAHASIA';
  SANGAT_RAHASIA: 'SANGAT_RAHASIA';
  KONFIDENSIAL: 'KONFIDENSIAL';
} = {
  BIASA: 'BIASA',
  RAHASIA: 'RAHASIA',
  SANGAT_RAHASIA: 'SANGAT_RAHASIA',
  KONFIDENSIAL: 'KONFIDENSIAL',
};

export type Trait =
  (typeof Trait)[keyof typeof Trait];

export type MailType =
  (typeof MailType)[keyof typeof MailType];

export type MailStatus =
  (typeof MailStatus)[keyof typeof MailStatus];

export class ActivityParticipationMailDto {
  @IsString()
  followUpFrom: string;

  @IsString()
  content: string;

  @IsOptional()
  fileAttachment?: string;

  @IsOptional()
  mailId?: number;
}

export class CircularMailDto {
  @IsString()
  content: string;

  @IsOptional()
  mailId?: number;
}

export class InvitationMailDto {
  @IsString()
  date: string;

  @IsString()
  startTime: string;

  @IsString()
  endTime: string;

  @IsString()
  place: string;

  @IsOptional()
  @IsString()
  clothes?: string;

  @IsString()
  content: string;

  @IsOptional()
  mailId?: number;
}

export class TaskOrderMailDto {
  @IsString()
  content: string;

  @IsOptional()
  @IsString()
  fileAttachment?: string;

  @IsOptional()
  mailId?: number;
}

export class OfficialNoteMailDto {
  @IsString()
  item: string;

  @IsNumber()
  quantity: number;

  @IsString()
  unit: string;

  @IsOptional()
  mailId: number;
}

export class CreateMailDto {
  @IsString()
  reference_number: string;

  @IsString()
  trait: Trait;

  @IsString()
  @IsOptional()
  date: string;

  @IsString()
  type: MailType;

  @IsString()
  status: MailStatus;

  @IsString()
  subject: string;

  @IsOptional()
  templateId: number;

  @IsOptional()
  @IsNumber()
  recipientId: number;

  @IsOptional()
  activityParticipationMail?: ActivityParticipationMailDto;

  @IsOptional()
  officialNoteMail?: OfficialNoteMailDto[];

  @IsOptional()
  taskOrderMail?: TaskOrderMailDto;

  @IsOptional()
  circularMail?: CircularMailDto;

  @IsOptional()
  invitationMail?: InvitationMailDto;
}

export class ArchieveMailDto {
  @IsNumber()
  mailBoxId: number;
}

export class UpdateMailDto {
  @IsOptional()
  @IsString()
  reference_number: string;

  @IsOptional()
  @IsString()
  trait: Trait;

  @IsOptional()
  @IsString()
  type: MailType;

  @IsOptional()
  @IsString()
  status: MailStatus;

  @IsOptional()
  @IsString()
  subject: string;

  @IsOptional()
  @IsString()
  revision: string;

  @IsOptional()
  templateId: number;

  @IsOptional()
  @IsNumber()
  recipientId: number;

  @IsOptional()
  @IsNumber()
  fromUserId: number;

  @IsOptional()
  activityParticipationMail?: ActivityParticipationMailDto;

  @IsOptional()
  officialNoteMail?: OfficialNoteMailDto[];

  @IsOptional()
  taskOrderMail?: TaskOrderMailDto;

  @IsOptional()
  circularMail?: CircularMailDto;

  @IsOptional()
  invitationMail?: InvitationMailDto;
}
