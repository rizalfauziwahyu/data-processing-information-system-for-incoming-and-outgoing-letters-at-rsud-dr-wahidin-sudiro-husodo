import { IsString } from 'class-validator';

export const Role: {
  USER: 'USER';
  STAFF: 'STAFF';
  ADMIN: 'ADMIN';
} = {
  USER: 'USER',
  STAFF: 'STAFF',
  ADMIN: 'ADMIN',
};

export type Role =
  (typeof Role)[keyof typeof Role];

export class SignUpDto {
  @IsString()
  username: string;

  @IsString()
  password: string;

  @IsString()
  name: string;

  @IsString()
  role: Role;
}

export class SignInDto {
  @IsString()
  username: string;

  @IsString()
  password: string;
}
