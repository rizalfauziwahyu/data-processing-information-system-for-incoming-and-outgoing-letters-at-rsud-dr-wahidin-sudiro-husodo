import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma';
import { Role } from './dto';

describe('AuthController', () => {
  let authController: AuthController;
  let prisma: PrismaService;

  const userData = [
    {
      username: 'usertestadmin',
      password: 'inkripsi',
      name: 'User Admin',
      role: Role.ADMIN,
    },
    {
      username: 'userteststaff',
      password: 'inkripsi',
      name: 'User Staff',
      role: Role.STAFF,
    },
    {
      username: 'usertestuser',
      password: 'inkripsi',
      name: 'User Division',
      role: Role.USER,
    },
  ];

  beforeAll(async () => {
    const moduleRef =
      await Test.createTestingModule({
        controllers: [AuthController],
        providers: [
          AuthService,
          ConfigService,
          JwtService,
          PrismaService,
        ],
      }).compile();

    authController =
      moduleRef.get<AuthController>(
        AuthController,
      );

    prisma = moduleRef.get<PrismaService>(
      PrismaService,
    );

    prisma.cleanDb();
  });

  afterAll(async () => {
    await prisma.cleanDb();
  });

  describe('signup', () => {
    it('Register User Admin', async () => {
      const result = await authController.signUp(
        userData[0],
      );
      expect(result).toHaveProperty('id');
      expect(result).toHaveProperty(
        'username',
        userData[0].username,
      );
      expect(result).toHaveProperty(
        'name',
        userData[0].name,
      );
      expect(result).toHaveProperty(
        'role',
        Role.ADMIN,
      );
      expect(result).toHaveProperty(
        'access_token',
      );
    });

    it('Register User Staff', async () => {
      const result = await authController.signUp(
        userData[1],
      );
      expect(result).toHaveProperty('id');
      expect(result).toHaveProperty(
        'username',
        userData[1].username,
      );
      expect(result).toHaveProperty(
        'name',
        userData[1].name,
      );
      expect(result).toHaveProperty(
        'role',
        Role.STAFF,
      );
      expect(result).toHaveProperty(
        'access_token',
      );
    });

    it('Register User', async () => {
      const result = await authController.signUp(
        userData[2],
      );
      expect(result).toHaveProperty('id');
      expect(result).toHaveProperty(
        'username',
        userData[2].username,
      );
      expect(result).toHaveProperty(
        'name',
        userData[2].name,
      );
      expect(result).toHaveProperty(
        'role',
        Role.USER,
      );
      expect(result).toHaveProperty(
        'access_token',
      );
    });

    it('should throw error if username already exists', async () => {
      try {
        await authController.signUp(userData[2]);
      } catch (error) {
        expect(error.message).toBe(
          'Username sudah digunakan!',
        );
      }
    });
  });
});
