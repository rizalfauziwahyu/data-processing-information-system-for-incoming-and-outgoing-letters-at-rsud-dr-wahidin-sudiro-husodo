import {
  Test,
  TestingModule,
} from '@nestjs/testing';
import { PrismaService } from 'src/prisma';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { Role } from './dto';

describe('AuthService', () => {
  let service: AuthService;
  let prisma: PrismaService;

  const userData = [
    {
      username: 'usertestadmin',
      password: 'inkripsi',
      name: 'User Admin',
      role: Role.ADMIN,
    },
    {
      username: 'userteststaff',
      password: 'inkripsi',
      name: 'User Staff',
      role: Role.STAFF,
    },
    {
      username: 'usertestuser',
      password: 'inkripsi',
      name: 'User Division',
      role: Role.USER,
    },
  ];

  beforeEach(async () => {
    const moduleRef: TestingModule =
      await Test.createTestingModule({
        providers: [
          AuthService,
          PrismaService,
          JwtService,
          ConfigService,
        ],
      }).compile();

    prisma = moduleRef.get<PrismaService>(
      PrismaService,
    );
    service =
      moduleRef.get<AuthService>(AuthService);

    await prisma.cleanDb();
  });

  afterAll(async () => {
    await prisma.cleanDb();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('SignUp', () => {
    it('register user admin', async () => {
      const signup = await service.signUp(
        userData[0],
      );

      expect(signup).toHaveProperty(
        'role',
        Role.ADMIN,
      );
      expect(signup).toHaveProperty(
        'access_token',
      );

      expect(signup).toMatchObject({
        name: 'User Admin',
        username: 'usertestadmin',
        profilePhoto: null,
        role: 'ADMIN',
      });
    });

    it('register user staff', async () => {
      const signup = await service.signUp(
        userData[1],
      );

      expect(signup).toHaveProperty(
        'role',
        Role.STAFF,
      );
      expect(signup).toHaveProperty(
        'access_token',
      );

      expect(signup).toMatchObject({
        username: 'userteststaff',
        profilePhoto: null,
        name: 'User Staff',
        role: Role.STAFF,
      });
    });

    it('register user division', async () => {
      const signup = await service.signUp(
        userData[2],
      );

      expect(signup).toHaveProperty(
        'role',
        Role.USER,
      );
      expect(signup).toHaveProperty(
        'access_token',
      );

      expect(signup).toMatchObject({
        username: 'usertestuser',
        profilePhoto: null,
        name: 'User Division',
        role: Role.USER,
      });
    });
  });

  describe('SignIn', () => {
    it('Login User Admin', async () => {
      await service.signUp(userData[0]);
      const signin = await service.signIn({
        username: userData[0].username,
        password: userData[0].password,
      });

      expect(signin).toHaveProperty(
        'access_token',
      );
    });

    it('Login User Staff', async () => {
      await service.signUp(userData[1]);
      const signin = await service.signIn({
        username: userData[1].username,
        password: userData[1].password,
      });

      expect(signin).toHaveProperty(
        'access_token',
      );
    });

    it('Login User', async () => {
      await service.signUp(userData[2]);
      const signin = await service.signIn({
        username: userData[2].username,
        password: userData[2].password,
      });

      expect(signin).toHaveProperty(
        'access_token',
      );
    });

    it('should throw error if username incorrect', async () => {
      try {
        await service.signIn({
          username: 'testusernameincorrect',
          password: userData[2].password,
        });
      } catch (error) {
        expect(error.message).toBe(
          'Username tidak terdaftar!',
        );
      }
    });

    it('should throw error is password incorrect', async () => {
      await service.signUp(userData[2]);
      try {
        const signin = await service.signIn({
          username: userData[2].username,
          password: 'incorrectpassword',
        });
      } catch (error) {
        expect(error.message).toBe(
          'Password Salah!',
        );
      }
    });
  });
});
