import {
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { PrismaService } from 'src/prisma';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { SignInDto, SignUpDto } from './dto';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwt: JwtService,
    private config: ConfigService,
  ) {}

  async signUp(dto: SignUpDto) {
    const saltOrRounds = 10;
    const hashedPassword = await bcrypt.hash(
      dto.password,
      saltOrRounds,
    );

    try {
      const user = await this.prisma.user.create({
        data: {
          username: dto.username,
          password: hashedPassword,
          name: dto.name,
          role: dto.role,
        },
      });

      const accessToken = await this.signToken(
        user.id,
        user.username,
      );

      delete user.password;

      return {
        ...user,
        ...accessToken,
      };
    } catch (error) {
      if (
        error instanceof
        PrismaClientKnownRequestError
      ) {
        if (error.code === 'P2002') {
          throw new ForbiddenException(
            'Username sudah digunakan!',
          );
        }
      }
    }
  }

  async signIn(dto: SignInDto) {
    const user =
      await this.prisma.user.findUnique({
        where: {
          username: dto.username,
        },
      });

    if (!user) {
      throw new ForbiddenException(
        'Username tidak terdaftar!',
      );
    }

    const pwMatches = await bcrypt.compare(
      dto.password,
      user.password,
    );

    if (!pwMatches) {
      throw new ForbiddenException(
        'Password Salah!',
      );
    }

    delete user.password;

    const accessToken = await this.signToken(
      user.id,
      user.username,
    );

    return {
      ...accessToken,
      ...user,
    };
  }

  async signToken(
    userId: number,
    username: string,
  ): Promise<{ access_token: string }> {
    const payload = {
      sub: userId,
      username,
    };

    const secret = this.config.get('JWT_SECRET');

    const token = await this.jwt.signAsync(
      payload,
      {
        expiresIn: '1440m',
        secret: secret,
      },
    );

    return {
      access_token: token,
    };
  }
}
