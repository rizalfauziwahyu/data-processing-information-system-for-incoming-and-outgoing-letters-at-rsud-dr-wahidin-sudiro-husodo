import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  UseGuards,
} from '@nestjs/common';
import { JwtGuard } from 'src/auth/guard';
import { UserService } from './user.service';
import { GetUser } from 'src/auth/decorator';
import { User } from '@prisma/client';
import { UpdateUserDto } from './dto';

@UseGuards(JwtGuard)
@Controller('api/users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('me')
  getMe(@GetUser() user: User) {
    return this.userService.findOne(user.id);
  }

  @Get('/all')
  getUsers() {
    return this.userService.findUsers();
  }

  @Patch(':id')
  updateUser(
    @GetUser() user: User,
    @Param('id', ParseIntPipe) userId: number,
    @Body() dto: UpdateUserDto,
  ) {
    return this.userService.updateUserById(
      user,
      userId,
      dto,
    );
  }

  @Delete(':id')
  deleteUser(
    @GetUser() user: User,
    @Param('id', ParseIntPipe) userId: number,
  ) {
    return this.userService.deleteUserById(
      user,
      userId,
    );
  }
}
