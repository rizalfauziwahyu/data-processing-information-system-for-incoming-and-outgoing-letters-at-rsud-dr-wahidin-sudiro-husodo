import {
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma';
import { UpdateUserDto } from './dto';
import { User } from '@prisma/client';
import { Role } from 'src/auth/dto';
import * as bcrypt from 'bcrypt';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  async findOne(userId: number) {
    return this.prisma.user.findUnique({
      where: {
        id: userId,
      },
      include: {
        division: true,
      },
    });
  }

  async findUsers() {
    return this.prisma.user.findMany({
      where: {
        role: Role.USER,
      },
    });
  }

  async updateUserById(
    user: User,
    userId: number,
    dto: UpdateUserDto,
  ) {
    try {
      if (this.isAdmin(user)) {
        dto = await this.hashPasswordIfExist(dto);

        return await this.prisma.user.update({
          where: {
            id: userId,
          },
          data: {
            ...dto,
          },
        });
      } else {
        const findUser =
          await this.prisma.user.findUnique({
            where: {
              id: userId,
            },
          });

        if (findUser.id !== userId)
          throw new ForbiddenException(
            'Anda tidak berhak melakukan aksi ini!',
          );

        dto = await this.hashPasswordIfExist(dto);

        return this.prisma.user.update({
          where: {
            id: findUser.id,
          },
          data: {
            ...dto,
          },
        });
      }
    } catch (error) {
      if (
        error instanceof
        PrismaClientKnownRequestError
      ) {
        if (error.code === 'P2002') {
          throw new ForbiddenException(
            'Username sudah digunakan!',
          );
        }
      }
      throw error;
    }
  }

  async deleteUserById(
    user: User,
    userId: number,
  ) {
    if (!this.isAdmin(user)) {
      throw new ForbiddenException(
        'Anda tidak berhak melakukan aksi ini!',
      );
    }

    return await this.prisma.user.delete({
      where: {
        id: userId,
      },
    });
  }

  private isAdmin(user: User): boolean {
    if (user.role === Role.ADMIN) return true;
    else return false;
  }

  private async hashPasswordIfExist(
    dto: UpdateUserDto,
  ) {
    if (dto.password) {
      dto.password = await bcrypt.hash(
        dto.password,
        10,
      );
    }

    return dto;
  }
}
