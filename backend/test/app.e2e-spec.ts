import {
  ValidationPipe,
  INestApplication,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as pactum from 'pactum';
import { AppModule } from 'src/app.module';
import {
  SignInDto,
  SignUpDto,
} from 'src/auth/dto';
import { PrismaService } from 'src/prisma';

describe('App e2e', () => {
  let app: INestApplication;
  let prisma: PrismaService;

  beforeAll(async () => {
    const moduleRef =
      await Test.createTestingModule({
        imports: [AppModule],
      }).compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      }),
    );

    await app.init();
    await app.listen(3333);

    prisma = app.get(PrismaService);
    await prisma.cleanDb();

    pactum.request.setBaseUrl(
      'http://localhost:3333',
    );
  });

  afterAll(() => {
    app.close();
  });

  describe('POST /auth/signup', () => {
    const dto: SignUpDto = {
      username: 'usertest',
      password: 'usertest',
      name: 'usertest',
      role: 'USER',
    };

    test.todo('should throw if email empty');

    test.todo('should throw is password empty');

    test.todo('should throw is no body');

    it('should return user object & access_token', async () => {
      return pactum
        .spec()
        .post('/auth/signup')
        .withBody(dto)
        .expectStatus(201)
        .inspect();
    });
  });
});
