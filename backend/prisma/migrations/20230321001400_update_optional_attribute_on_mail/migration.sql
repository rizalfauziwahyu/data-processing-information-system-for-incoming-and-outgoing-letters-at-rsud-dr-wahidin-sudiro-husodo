-- AlterTable
ALTER TABLE "activity_participation_mails" ALTER COLUMN "file_attachment" DROP NOT NULL;

-- AlterTable
ALTER TABLE "invitation_mails" ALTER COLUMN "clothes" DROP NOT NULL;
