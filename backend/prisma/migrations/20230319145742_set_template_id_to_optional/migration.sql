-- DropForeignKey
ALTER TABLE "mails" DROP CONSTRAINT "mails_template_id_fkey";

-- AlterTable
ALTER TABLE "mails" ALTER COLUMN "template_id" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "mails" ADD CONSTRAINT "mails_template_id_fkey" FOREIGN KEY ("template_id") REFERENCES "mail_templates"("id") ON DELETE SET NULL ON UPDATE CASCADE;
