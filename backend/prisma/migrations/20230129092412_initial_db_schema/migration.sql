-- CreateEnum
CREATE TYPE "MailStatus" AS ENUM ('PENDING', 'VERIFIED', 'REVISION', 'PROCESS', 'DRAFT');

-- CreateEnum
CREATE TYPE "MailType" AS ENUM ('OFFICIALNOTEMAIL', 'TASKORDERMAIL', 'INVITATIONMAIL', 'CIRCULARMAIL', 'ACTIVITYPARTICIPATION');

-- CreateEnum
CREATE TYPE "Trait" AS ENUM ('BIASA', 'RAHASIA', 'SANGAT_RAHASIA', 'KONFIDENSIAL');

-- CreateEnum
CREATE TYPE "Role" AS ENUM ('ADMIN', 'STAFF', 'USER');

-- CreateTable
CREATE TABLE "divisions" (
    "id" SERIAL NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "description" TEXT,
    "user_id" INTEGER,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "divisions_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "division_members" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "division_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "division_members_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "users" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "username" VARCHAR(100) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "profile_photo" VARCHAR(255),
    "role" "Role" NOT NULL DEFAULT 'USER',
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "mails" (
    "id" SERIAL NOT NULL,
    "reference_number" VARCHAR(100) NOT NULL,
    "trait" "Trait" NOT NULL DEFAULT 'BIASA',
    "subject" VARCHAR(255) NOT NULL,
    "status" "MailStatus" NOT NULL,
    "type" "MailType" NOT NULL,
    "revisions" TEXT,
    "template_id" INTEGER NOT NULL,
    "from_user_id" INTEGER NOT NULL,
    "recipient_id" INTEGER,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "mails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "mail_templates" (
    "id" SERIAL NOT NULL,
    "file_template" VARCHAR(255) NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "mail_templates_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "official_note_mails" (
    "id" SERIAL NOT NULL,
    "item" VARCHAR(255) NOT NULL,
    "quantity" INTEGER NOT NULL,
    "unit" VARCHAR(100) NOT NULL,
    "mail_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "official_note_mails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "task_order_mails" (
    "id" SERIAL NOT NULL,
    "content" TEXT NOT NULL,
    "file_attachment" VARCHAR(255),
    "mail_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "task_order_mails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "circular_mails" (
    "id" SERIAL NOT NULL,
    "content" TEXT NOT NULL,
    "mail_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "circular_mails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "invitation_mails" (
    "id" SERIAL NOT NULL,
    "date" DATE NOT NULL,
    "start_time" TIME NOT NULL,
    "end_time" TIME NOT NULL,
    "place" VARCHAR(255) NOT NULL,
    "clothes" VARCHAR(100) NOT NULL,
    "content" TEXT NOT NULL,
    "mail_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "invitation_mails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "activity_participation_mails" (
    "id" SERIAL NOT NULL,
    "follow_up_from" TEXT NOT NULL,
    "content" TEXT NOT NULL,
    "file_attachment" VARCHAR(255) NOT NULL,
    "mail_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "activity_participation_mails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "mail_histories" (
    "id" SERIAL NOT NULL,
    "division" VARCHAR(255) NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    "destination" VARCHAR(255) NOT NULL,
    "mail_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "mail_histories_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "mailboxes" (
    "id" SERIAL NOT NULL,
    "disposition_to" VARCHAR(255),
    "mail_id" INTEGER NOT NULL,
    "from_user_id" INTEGER NOT NULL,
    "recipient_id" INTEGER,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "mailboxes_pkey" PRIMARY KEY ("mail_id","from_user_id","id")
);

-- CreateTable
CREATE TABLE "mail_reads" (
    "is_read" BOOLEAN NOT NULL,
    "mail_box_id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "mail_reads_pkey" PRIMARY KEY ("mail_box_id","user_id")
);

-- CreateTable
CREATE TABLE "mail_archives" (
    "is_archieved" BOOLEAN NOT NULL,
    "mail_box_id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "mail_archives_pkey" PRIMARY KEY ("mail_box_id","user_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "divisions_id_key" ON "divisions"("id");

-- CreateIndex
CREATE UNIQUE INDEX "divisions_user_id_key" ON "divisions"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "division_members_id_key" ON "division_members"("id");

-- CreateIndex
CREATE UNIQUE INDEX "users_id_key" ON "users"("id");

-- CreateIndex
CREATE UNIQUE INDEX "mails_id_key" ON "mails"("id");

-- CreateIndex
CREATE UNIQUE INDEX "mail_templates_id_key" ON "mail_templates"("id");

-- CreateIndex
CREATE UNIQUE INDEX "official_note_mails_id_key" ON "official_note_mails"("id");

-- CreateIndex
CREATE UNIQUE INDEX "official_note_mails_mail_id_key" ON "official_note_mails"("mail_id");

-- CreateIndex
CREATE UNIQUE INDEX "task_order_mails_id_key" ON "task_order_mails"("id");

-- CreateIndex
CREATE UNIQUE INDEX "task_order_mails_mail_id_key" ON "task_order_mails"("mail_id");

-- CreateIndex
CREATE UNIQUE INDEX "circular_mails_id_key" ON "circular_mails"("id");

-- CreateIndex
CREATE UNIQUE INDEX "circular_mails_mail_id_key" ON "circular_mails"("mail_id");

-- CreateIndex
CREATE UNIQUE INDEX "invitation_mails_id_key" ON "invitation_mails"("id");

-- CreateIndex
CREATE UNIQUE INDEX "invitation_mails_mail_id_key" ON "invitation_mails"("mail_id");

-- CreateIndex
CREATE UNIQUE INDEX "activity_participation_mails_id_key" ON "activity_participation_mails"("id");

-- CreateIndex
CREATE UNIQUE INDEX "activity_participation_mails_mail_id_key" ON "activity_participation_mails"("mail_id");

-- CreateIndex
CREATE UNIQUE INDEX "mail_histories_id_key" ON "mail_histories"("id");

-- CreateIndex
CREATE UNIQUE INDEX "mailboxes_id_key" ON "mailboxes"("id");

-- CreateIndex
CREATE UNIQUE INDEX "mailboxes_mail_id_from_user_id_key" ON "mailboxes"("mail_id", "from_user_id");

-- CreateIndex
CREATE UNIQUE INDEX "mail_reads_mail_box_id_user_id_key" ON "mail_reads"("mail_box_id", "user_id");

-- CreateIndex
CREATE UNIQUE INDEX "mail_archives_mail_box_id_user_id_key" ON "mail_archives"("mail_box_id", "user_id");

-- AddForeignKey
ALTER TABLE "divisions" ADD CONSTRAINT "divisions_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "division_members" ADD CONSTRAINT "division_members_division_id_fkey" FOREIGN KEY ("division_id") REFERENCES "divisions"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mails" ADD CONSTRAINT "mails_template_id_fkey" FOREIGN KEY ("template_id") REFERENCES "mail_templates"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mails" ADD CONSTRAINT "mails_from_user_id_fkey" FOREIGN KEY ("from_user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mails" ADD CONSTRAINT "mails_recipient_id_fkey" FOREIGN KEY ("recipient_id") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "official_note_mails" ADD CONSTRAINT "official_note_mails_mail_id_fkey" FOREIGN KEY ("mail_id") REFERENCES "mails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "task_order_mails" ADD CONSTRAINT "task_order_mails_mail_id_fkey" FOREIGN KEY ("mail_id") REFERENCES "mails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "circular_mails" ADD CONSTRAINT "circular_mails_mail_id_fkey" FOREIGN KEY ("mail_id") REFERENCES "mails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "invitation_mails" ADD CONSTRAINT "invitation_mails_mail_id_fkey" FOREIGN KEY ("mail_id") REFERENCES "mails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "activity_participation_mails" ADD CONSTRAINT "activity_participation_mails_mail_id_fkey" FOREIGN KEY ("mail_id") REFERENCES "mails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mail_histories" ADD CONSTRAINT "mail_histories_mail_id_fkey" FOREIGN KEY ("mail_id") REFERENCES "mails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mailboxes" ADD CONSTRAINT "mailboxes_mail_id_fkey" FOREIGN KEY ("mail_id") REFERENCES "mails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mailboxes" ADD CONSTRAINT "mailboxes_from_user_id_fkey" FOREIGN KEY ("from_user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mailboxes" ADD CONSTRAINT "mailboxes_recipient_id_fkey" FOREIGN KEY ("recipient_id") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mail_reads" ADD CONSTRAINT "mail_reads_mail_box_id_fkey" FOREIGN KEY ("mail_box_id") REFERENCES "mailboxes"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mail_reads" ADD CONSTRAINT "mail_reads_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mail_archives" ADD CONSTRAINT "mail_archives_mail_box_id_fkey" FOREIGN KEY ("mail_box_id") REFERENCES "mailboxes"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "mail_archives" ADD CONSTRAINT "mail_archives_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
