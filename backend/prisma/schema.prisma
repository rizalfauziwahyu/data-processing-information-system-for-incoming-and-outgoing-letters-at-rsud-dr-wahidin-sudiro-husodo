// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model Division {
  id          Int              @id @unique @default(autoincrement()) @map("id")
  title       String           @map("title") @db.VarChar(255)
  description String?          @map("description") @db.Text
  userId      Int?             @unique @map("user_id")
  user        User?            @relation(fields: [userId], references: [id])
  members     DivisionMember[]

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("divisions")
}

model DivisionMember {
  id         Int      @id @unique @default(autoincrement()) @map("id")
  name       String   @map("name") @db.VarChar(255)
  division   Division @relation(fields: [divisionId], references: [id])
  divisionId Int      @map("division_id")

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("division_members")
}

model User {
  id             Int           @id @unique @default(autoincrement()) @map("id")
  name           String        @map("name") @db.VarChar(255)
  divisionName   String?       @map("division_name")
  username       String        @unique @map("username") @db.VarChar(100)
  password       String        @map("password") @db.VarChar(255)
  profilePhoto   String?       @map("profile_photo") @db.VarChar(255)
  role           Role          @default(USER) @map("role")
  division       Division?
  mailRecipients Mail[]        @relation(name: "mail_recipients")
  mailFroms      Mail[]        @relation(name: "mails_from")
  inboxes        MailBox[]     @relation(name: "inboxes")
  sents          MailBox[]     @relation(name: "sents")
  mailReads      MailRead[]
  mailArchieves  MailArchive[]

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("users")
}

model Mail {
  id                        Int                        @id @unique @default(autoincrement()) @map("id")
  referenceNumber           String                     @map("reference_number") @db.VarChar(100)
  trait                     Trait                      @default(BIASA) @map("trait")
  subject                   String                     @map("subject") @db.VarChar(255)
  status                    MailStatus                 @map("status")
  type                      MailType                   @map("type")
  revision                  String?                    @map("revisions") @db.Text
  templateId                Int?                       @map("template_id")
  fromUserId                Int                        @map("from_user_id")
  recipientId               Int?                       @map("recipient_id")
  date                      String?
  template                  MailTemplate?              @relation(fields: [templateId], references: [id])
  from                      User                       @relation(fields: [fromUserId], references: [id], name: "mails_from")
  recipient                 User?                      @relation(fields: [recipientId], references: [id], name: "mail_recipients")
  officialNoteMails         OfficialNoteMail[]
  taskOrderMail             TaskOrderMail?
  circularMail              CircularMail?
  invitationMail            InvitationMail?
  activityParticipationMail ActivityParticipationMail?
  histories                 MailHistory[]
  mailBox                   MailBox[]

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("mails")
}

model MailTemplate {
  id           Int    @id @unique @default(autoincrement()) @map("id")
  fileTemplate String @map("file_template") @db.VarChar(255)
  mails        Mail[]

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("mail_templates")
}

model OfficialNoteMail {
  id       Int    @id @unique @default(autoincrement()) @map("id")
  item     String @map("item") @db.VarChar(255)
  quantity Int    @map("quantity")
  unit     String @map("unit") @db.VarChar(100)
  mailId   Int    @map("mail_id")
  mail     Mail   @relation(fields: [mailId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("official_note_mails")
}

model TaskOrderMail {
  id             Int     @id @unique @default(autoincrement()) @map("id")
  content        String  @map("content") @db.Text
  fileAttachment String? @map("file_attachment") @db.VarChar(255)
  mailId         Int     @unique @map("mail_id")
  mail           Mail    @relation(fields: [mailId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("task_order_mails")
}

model CircularMail {
  id      Int    @id @unique @default(autoincrement()) @map("id")
  content String @map("content") @db.Text
  mailId  Int    @unique @map("mail_id")
  mail    Mail   @relation(fields: [mailId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("circular_mails")
}

model InvitationMail {
  id        Int      @id @unique @default(autoincrement()) @map("id")
  date      DateTime @map("date") @db.Date
  startTime DateTime @map("start_time") @db.Time()
  endTime   DateTime @map("end_time") @db.Time()
  place     String   @map("place") @db.VarChar(255)
  clothes   String?  @map("clothes") @db.VarChar(100)
  content   String   @map("content") @db.Text
  mailId    Int      @unique @map("mail_id")
  mail      Mail     @relation(fields: [mailId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("invitation_mails")
}

model ActivityParticipationMail {
  id             Int     @id @unique @default(autoincrement()) @map("id")
  followUpFrom   String  @map("follow_up_from") @db.Text
  content        String  @map("content") @db.Text
  fileAttachment String? @map("file_attachment") @db.VarChar(255)
  mailId         Int     @unique @map("mail_id")
  mail           Mail    @relation(fields: [mailId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("activity_participation_mails")
}

model MailHistory {
  id          Int    @id @unique @default(autoincrement()) @map("id")
  division    String @map("division") @db.VarChar(255)
  name        String @map("name") @db.VarChar(255)
  description String @map("description") @db.VarChar(255)
  destination String @map("destination") @db.VarChar(255)
  mailId      Int    @map("mail_id")
  mail        Mail   @relation(fields: [mailId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@map("mail_histories")
}

model MailBox {
  id            Int           @unique @default(autoincrement()) @map("id")
  dispositionTo String?       @map("disposition_to") @db.VarChar(255)
  mailId        Int           @map("mail_id")
  fromUserId    Int           @map("from_user_id")
  recipientId   Int?          @map("recipient_id")
  mail          Mail          @relation(fields: [mailId], references: [id])
  from          User          @relation(fields: [fromUserId], references: [id], name: "inboxes")
  recipient     User?         @relation(fields: [recipientId], references: [id], name: "sents")
  mailRead      MailRead[]
  mailArchive   MailArchive[]

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@id([mailId, fromUserId, id])
  @@unique([mailId, fromUserId])
  @@map("mailboxes")
}

model MailRead {
  isRead    Boolean @map("is_read")
  mailBoxId Int     @map("mail_box_id")
  userId    Int     @map("user_id")
  mailBox   MailBox @relation(fields: [mailBoxId], references: [id])
  user      User    @relation(fields: [userId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@id([mailBoxId, userId])
  @@unique([mailBoxId, userId])
  @@map("mail_reads")
}

model MailArchive {
  isArchieved Boolean @map("is_archieved")
  mailBoxId   Int     @map("mail_box_id")
  userId      Int     @map("user_id")
  mailBox     MailBox @relation(fields: [mailBoxId], references: [id])
  user        User    @relation(fields: [userId], references: [id])

  createdAt DateTime @default(now()) @map("created_at")
  updatedAt DateTime @updatedAt @map("updated_at")

  @@id([mailBoxId, userId])
  @@unique([mailBoxId, userId])
  @@map("mail_archives")
}

enum MailStatus {
  PENDING
  VERIFIED
  REVISION
  PROCESS
  DRAFT
  ARCHIEVED
}

enum MailType {
  OFFICIALNOTEMAIL
  TASKORDERMAIL
  INVITATIONMAIL
  CIRCULARMAIL
  ACTIVITYPARTICIPATION
}

enum Trait {
  BIASA
  RAHASIA
  SANGAT_RAHASIA
  KONFIDENSIAL
}

enum Role {
  ADMIN
  STAFF
  USER
}
