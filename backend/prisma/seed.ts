import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { Role } from '@prisma/client';

const division = [
  'UMUM',
  'Direktur',
  'POLI ANAK',
  'POLI GIGI',
];

export async function cAdminUser() {
  const name = 'admin';
  const username = 'admin';
  return {
    name: `${name}`,
    username: `${username}`,
    role: Role.ADMIN,
    password: await bcrypt.hash('password', 10),
  };
}

export async function cStaffUser() {
  const name = 'staff';
  const username = 'staff';
  return {
    name: `${name}-${Math.random()
      .toString(36)
      .slice(2, 7)}`,
    username: `${username}-${Math.random()
      .toString(36)
      .slice(2, 7)}`,
    role: Role.STAFF,
    password: await bcrypt.hash('password', 10),
  };
}

export async function cUser() {
  const name = 'user';
  const username = 'user';
  return {
    name: `${name}-${Math.random()
      .toString(36)
      .slice(2, 7)}`,
    username: `${username}-${Math.random()
      .toString(36)
      .slice(2, 7)}`,
    role: Role.USER,
    password: await bcrypt.hash('password', 10),
  };
}

async function main() {
  const prisma = new PrismaClient();

  // clean db
  await prisma.$transaction([
    prisma.division.deleteMany(),
    prisma.user.deleteMany(),
  ]);

  // create departments
  for (const dep of division) {
    await prisma.division.create({
      data: {
        title: dep,
      },
    });
  }

  // create admin
  const adminData = await cAdminUser();
  const adminUser = await prisma.user.create({
    data: adminData,
  });

  // create staff
  const staffData = await cStaffUser();
  const staffUser = await prisma.user.create({
    data: staffData,
  });

  // create user division
  const divisions =
    await prisma.division.findMany();

  for (const dbDep of divisions) {
    const userData = await cUser();
    const user = await prisma.user.create({
      data: userData,
    });

    await prisma.division.update({
      where: {
        id: dbDep.id,
      },
      data: {
        userId: user.id,
      },
    });
  }

  // create division admin
  await prisma.division.create({
    data: {
      title: 'ADMIN',
      userId: adminUser.id,
    },
  });

  // create division staff
  await prisma.division.create({
    data: {
      title: 'STAFF',
      userId: staffUser.id,
    },
  });
}

main();
